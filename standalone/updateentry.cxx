#include "ers/ers.h"
#include "SFOTZ/RunTable.h"
#include "SFOTZ/LumiBlockTable.h"
#include "SFOTZ/FileTable.h"
#include <boost/tokenizer.hpp>
#include <boost/program_options.hpp>
#include <string>
#include <iostream>
#include <vector>

namespace po = boost::program_options;

void parse_filename(std::string file,
    unsigned int& runnr,
    unsigned int& lbnr,
    unsigned int& filenr,
    std::string& streamtype,
    std::string& stream,
    std::string& app){

  typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
  
  boost::char_separator<char> sep(".","",boost::keep_empty_tokens);
  
  tokenizer tokens(file, sep);
  tokenizer::iterator tok_iter = tokens.begin();
  tok_iter++; //daq
  tok_iter++; //tag
  std::string runs = *tok_iter;
  tok_iter++;
  streamtype = *tok_iter;
  tok_iter++;
  stream = *tok_iter;
  tok_iter++;
  std::string lbs = *tok_iter;
  tok_iter++;
  app = *tok_iter;
  tok_iter++;
  std::string filenrs = *tok_iter;
  tok_iter++;
  std::string ext = *tok_iter;
  tok_iter++;
	
  runnr = atoi(runs.c_str());
  lbnr = atoi(lbs.substr(2).c_str());
  filenr = atoi(filenrs.substr(1).c_str());
  if(stream=="") stream="null";
}

int main(int argc, char *argv[]){
  
  std::string conn;
  std::string rname, lname, fname;
  std::string oper;
  std::vector<std::string> sfofiles,castorfiles;

  try {
    po::options_description desc("Update the state of the files given as input. If it is the case, the status of lumiblock and run tables is updated\n"
                                 " In the environment you need:\n CORAL_AUTH_USER=username\n CORAL_AUTH_PASSWORD=password\n TNS_ADMIN=oracle lookup file");

    desc.add_options()
      ("help,h", "Print help message")
      ("connection,c", po::value<std::string>(&conn)->required(), "Connection string (e.g oracle://atonr_conf/ATLAS_SFO_T0)")
      ("run,r", po::value<std::string>(&rname)->required(), "Name of the run table")
      ("lumi,l", po::value<std::string>(&lname)->required(), "Name of the lumi table")
      ("file,f", po::value<std::string>(&fname)->required(), "Name of the file table")
      ("operation,t", po::value<std::string>(&oper)->required(), "Operation to be performed:\nadd-file\nclose-file\nadd-lb\nclose-lb\nadd-run\nclose-run\nfile-transfer\nfile-deletion")
      ("sfofilenames,s", po::value<std::vector<std::string>>(&sfofiles)->required(),"List of files to be updated (complete names and path on disk)")
      ("castorfilenames,p",po::value<std::vector<std::string>>(&castorfiles)->required(),"Name and complete path of files on castor (one-to-one correspondence with the previous list). Required for transfer operation, neglected for delete operation")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);

    if (vm.count("help")) {
      std::cout << desc << std::endl;
      return 0;
    }

    po::notify(vm);

  } catch (std::exception& ex) {
    std::cout << "Parsing failed" << std::endl;
    return 1;
  }


  if(oper == "transfer" &&
      sfofiles.size()!=castorfiles.size()){
    ERS_INFO("Transfer operation requested, but the sizes of two lists are" 
        " different. #Transferred files = " << sfofiles.size()
        << "#File names on destination = " << castorfiles.size()
        << ". Exiting");
    return 1;
  }

  SFOTZ::DBConnection * db;
  SFOTZ::RunTable * runt;
  SFOTZ::LumiBlockTable * lumit;
  SFOTZ::FileTable * filet;

  try{
    db =  new SFOTZ::DBConnection(std::string(conn));
  }catch(SFOTZ::DBConnectionIssue& iss){
    ers::error(iss);
    return 1;
  }

  std::unique_ptr<SFOTZ::DBConnection> dbconn(db);

  try{
    runt = new SFOTZ::RunTable(*db,rname);
  }catch(SFOTZ::TableIssue & ex){
    ers::error(ex);
    return 1;
  }
  
  std::unique_ptr<SFOTZ::RunTable> runtable(runt);
  
  try{
    lumit = new SFOTZ::LumiBlockTable(*db,lname);
  }catch(SFOTZ::TableIssue & ex){
    ers::error(ex);
    return 1;
  } 
  
  std::unique_ptr<SFOTZ::LumiBlockTable> lumiblocktable(lumit);
  
  try{
    filet = new SFOTZ::FileTable(*db,std::string(fname));
  }catch(SFOTZ::TableIssue & ex){
    ers::error(ex);
    return 1;
  }
  
  std::unique_ptr<SFOTZ::FileTable> filetable(filet);
  
  SFOTZ::Transaction * tr = new SFOTZ::Transaction(*db);

  try{
    
    if(oper == "file-transfer"){
      std::vector<std::string>::iterator sIt = sfofiles.begin();
      std::vector<std::string>::iterator cIt = castorfiles.begin();
      for(; sIt != sfofiles.end(); ){
        filet->setFileEntryTransferred(*sIt,*cIt,true);

        unsigned int runnr;
        unsigned int lbnr;
        unsigned int filenr;
        std::string streamtype;
        std::string stream;
        std::string app;

        parse_filename(*sIt, runnr, lbnr, filenr, streamtype, stream,app);

        delete tr;
        tr = new SFOTZ::Transaction(*db);

        if(filet->allFileTransferred(app,
            runnr,
            lbnr,
            streamtype,
            stream,
            true)){

            lumit->setLumiBlockEntryTransferred(app,
                runnr,
                lbnr,
                streamtype,
                stream,
                true);

            delete tr;
            tr = new SFOTZ::Transaction(*db);

            if(lumit->allLumiBlockTransferred(app,
                runnr,
                streamtype,
                stream,
                true)){

                runt->setRunEntryTransferred(app,
                    runnr,
                    streamtype,
                    stream,
                    true);
            }

        }
        sIt++;
        cIt++;
      }

    }else if(oper == "file-deletion"){
      for(std::vector<std::string>::iterator it = sfofiles.begin();
          it != sfofiles.end(); it++){
        filet->setFileEntryDeleted(*it,true);
      }
    }else if(oper == "add-file"){

    }else if(oper == "close-file"){

    }else if(oper == "add-lb"){

    }else if(oper == "close-lb"){

    }else if(oper == "add-run"){

    }else if(oper == "close-run"){

    }else{
      ERS_INFO("Unknown operation: " << oper);
      return 1;
    }


  }catch(SFOTZ::SFOTZIssue & ex){
    tr->rollback();
    delete tr;
    ers::error(ex);
    return 1;
  }

  delete tr;
  
  return 0;
}
