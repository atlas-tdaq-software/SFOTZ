#include "ers/ers.h"
#include "SFOTZ/IndexTable.h"
#include "SFOTZ/RunTable.h"
#include "SFOTZ/LumiBlockTable.h"
#include "SFOTZ/FileTable.h"
#include "SFOTZ/OverlapTable.h"
#include <boost/program_options.hpp>
#include <string>
#include <iostream>

namespace po = boost::program_options;

int main(int argc, char *argv[]){


  std::string type;
  std::string tname;
  std::string conn;

  try {
    po::options_description desc("Create a new table. In the environment you need:\n CORAL_AUTH_USER=username\n CORAL_AUTH_PASSWORD=password\n TNS_ADMIN=oracle lookup file\nOptions");
    desc.add_options()
      ("help,h", "Print help message")
      ("connection,c", po::value<std::string>(&conn)->required(), "Connection string (e.g oracle://atonr_conf/ATLAS_SFO_T0)")
      ("type,t", po::value<std::string>(&type)->required(), "Type of the table to be created: run, lumi, file, index, overlap")
      ("name,n", po::value<std::string>(&tname)->required(), "Type of the table to be created: run, lumi, file, index, overlap")
      ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);

    if (vm.count("help")) {
      std::cout << desc << std::endl;
      return 0;
    }

    po::notify(vm);

  } catch (std::exception& ex) {
    std::cout << "Parsing failed" << std::endl;
    return 1;
  }

  SFOTZ::DBConnection * db;
  try{
    db =  new SFOTZ::DBConnection(conn);
  }catch(SFOTZ::DBConnectionIssue& iss){
    ers::error(iss);
    return 1;
  }
  
    
  try{
    if(type=="run"){
      SFOTZ::RunTable run(*db,tname,true);
    }else if(type=="lumi"){
      SFOTZ::LumiBlockTable lumi(*db,tname,true);
    }else if(type=="file"){
      SFOTZ::FileTable file(*db,tname,true);
    }else if(type=="index"){
      SFOTZ::IndexTable index(*db,tname,true);
    }else if(type=="overlap"){
      SFOTZ::OverlapTable overlap(*db,tname,true);
    }else{
      SFOTZ::TableIssue issue(ERS_HERE, "Unknown table type.");
      ers::error(issue);
      delete db;
      return 1;
    }
  }catch(SFOTZ::SFOTZIssue & ex){
    SFOTZ::TableIssue issue(ERS_HERE, "cannot create table.",ex);
    ers::error(issue);
    delete db;
    return 1;
  }

  delete db;
  
  return 0;
}
