/**
 * @file FileTable.h
 * @author <a href="mailto:Wainer.Vandelli@cern.ch">Wainer Vandelli</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Generic FileTable description
 */

#ifndef SFOTZ_FILETABLE_H
#define SFOTZ_FILETABLE_H

#include "SFOTZ/Table.h"
#include "SFOTZ/IndexTable.h"
#include <vector>
#include <string>

namespace SFOTZ{
  
  /*namespace File{
    enum HealthStates{SANE, TRUNCATED};
    enum TransferStates{ONDISK, TRANSFERRED, STAGED};
    enum FileStates{OPENED, CLOSED, DELETED};
    }*/

  class FileTable: public Table{

  public:
    FileTable(DBConnection & db, const std::string& tablename, 
	     bool createifmissing=false);
    virtual ~FileTable();

    /**
     * Add a single opened entry to the file table. First
     * checks if the entry already exists, in this 
     * case the operation is skipped
     */
    void addOpenedFileEntry(const std::string& lfn,
			    const unsigned int& filenr,
			    const std::string& sfo,
			    const std::string& sfohost,
			    const unsigned int& runnr,
			    const unsigned int& lbnr,
			    const std::string& streamtype,
			    const std::string& stream,
			    const std::string& guid,
			    const std::string& sfopfn,
			    SFOTZ::IndexTable * index,
			    bool transactive=false);

    /**
     * Sets a single entry closed if and only if
     * its present state is opened
     */
    void setFileEntryClosed(const std::string& lfn,
			    const unsigned int& filenr,
			    const std::string& sfo,
			    const unsigned int& runnr,
			    const unsigned int& lbnr,
			    const std::string& streamtype,
			    const std::string& stream,
			    const uint64_t& filesize,
			    const std::string& checksum,
			    const unsigned int& nrevents,
			    const std::string& sfopfn,
			    bool transactive=false);
    
    /**
     * Sets a single entry transferred if and only if
     * its present state is ondisk
     */
    void setFileEntryTransferred(const std::string& lfn,
				 const std::string& pfn,
				 bool transactive=false);
    
     /**
      * Sets a single entry deleted if and only if
      * its present state is closed
      */
    void setFileEntryDeleted(const std::string& lfn,
			     bool transactive=false);
    
    /**
     * Sets a single entry closed and truncated if and only if
     * its present state is opened
     */
    void setFileEntryTruncated(const std::string& lfn,
			       const unsigned int& filenr,
			       const std::string& sfo,
			       const unsigned int& runnr,
			       const unsigned int& lbnr,
			       const std::string& streamtype,
			       const std::string& stream,
			       const uint64_t& filesize,
			       const std::string& sfopfn,
			       bool transactive=false);
       
    //Queries
    bool allFileTransferred(const std::string& sfo,
			    const unsigned int& runnr,
			    const unsigned int& lbnr,
			    const std::string& streamtype,
			    const std::string& stream,
			    bool transactive=false);

    bool allLBFileClosed(const std::string& sfo,
			 const unsigned int& runnr,
			 const unsigned int& lbnr,
			 bool transactive=false);


    //Returns a vector of complete paths on the SFO disks
    std::vector<std::string> 
      getNotClosedAlreadyExistingFiles(const std::string& sfo,
				       const unsigned int& runnr,
				       bool transactive=false);
    
    unsigned int
      getMaxIndex(const std::string& sfo,
		  const unsigned int& runnr,
		  const unsigned int& lbnr,
		  const std::string& streamtype,
		  const std::string& stream,
		  bool transactive=false);

    
  private:
    static coral::TableDescription * 
      createDescription(const std::string& tablename);

    //Queries
    bool alreadyExist(const unsigned int& filenr,
		      const std::string& sfo,
		      const unsigned int& runnr,
		      const unsigned int& lbnr,
		      const std::string& streamtype,
		      const std::string& stream,
		      bool transactive=false);

    bool alreadyExist(const std::string& lfn,
		      bool transactive=false);

  };
}
#endif
