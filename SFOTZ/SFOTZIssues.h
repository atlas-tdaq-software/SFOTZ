
#ifndef SFOTO_SFOTZISSUES_H
#define SFOTO_SFOTZISSUES_H

#include "ers/ers.h"
#include "CoralBase/Exception.h"

ERS_DECLARE_ISSUE(SFOTZ,             
		  SFOTZIssue,
		  ERS_EMPTY,ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE(SFOTZ,             
		       DBConnectionIssue, 
		       SFOTZ::SFOTZIssue,
		       "DB Connection failed because of: " << reason,
		       ERS_EMPTY,
		       ((const char *)reason )    
		       )

ERS_DECLARE_ISSUE_BASE(SFOTZ,             
		       TableIssue, 
		       SFOTZ::SFOTZIssue,
		       "Table handling problem: " << reason,
		       ERS_EMPTY,
		       ((const char *)reason )    
		       )

ERS_DECLARE_ISSUE_BASE(SFOTZ,             
		       TransactionIssue, 
		       SFOTZ::SFOTZIssue,
		       "Transaction problem: " << reason,
		       ERS_EMPTY,
		       ((const char *)reason )    
		       )

ERS_DECLARE_ISSUE_BASE(SFOTZ,
		       RuntimeIssue,
		       SFOTZ::SFOTZIssue,
		       "Runtime problem: " << reason,
		       ERS_EMPTY,
		       ((const char*) reason)
		       )


#endif
