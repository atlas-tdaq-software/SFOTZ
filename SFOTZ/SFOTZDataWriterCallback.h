/**
 * @file SFODataWriterCallback.h
 * @author <a href="mailto:Wainer.Vandelli@cern.ch">Wainer Vandelli</a> 
 * @author <a href="mailto:Andreas.Battaglia@cern.ch">Andreas Battaglia</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Describes the action that can be taken when an SFO closes or opens a file
 */

#ifndef SFOTZ_DATAWRITER_CALLBACK_H
#define SFOTZ_DATAWRITER_CALLBACK_H

#include <string>
#include <fstream>
#include <vector>
#include <sys/uio.h>
#include "EventStorage/DataWriterCallBack.h"
#include "SFOTZ/SFOTZThread.h"
  

namespace SFOTZ{

  class SFOTZDataWriterCallback: public EventStorage::DataWriterCallBack {

  public:
    SFOTZDataWriterCallback(std::string appName, std::vector<SFOTZThread *> dbhandlers);
    SFOTZDataWriterCallback(std::string appName, SFOTZThread * dbhandler);
    virtual ~SFOTZDataWriterCallback();
    
    void AddHandler(SFOTZThread * dbhandler){m_dbhandlers.push_back(dbhandler);}

    /** will be called after a successful file opening */
    virtual void FileWasOpened(std::string logicalfileName,
			       std::string fileName,
			       std::string streamtype,
			       std::string streamname,
			       std::string sfoid,
			       std::string guid,
			       unsigned int runNumber,
			       unsigned int filenumber,
			       unsigned int lumiblock);
    
    /** will be called after a successful file closing */
    virtual void FileWasClosed(std::string logicalfileName,
			       std::string fileName,
			       std::string streamtype,
			       std::string streamname,
			       std::string sfoid,
			       std::string guid,
			       std::string checksum,
			       unsigned int eventsInFile,
			       unsigned int runNumber,
			       unsigned int filenumber,
			       unsigned int lumiblock,
			       uint64_t filesize);
  
  private:
    
    void Hostname();

  private:
    std::string m_appName;
    std::vector<SFOTZThread *> m_dbhandlers;
    std::string m_hostName;
  };

}
#endif // SFOTZ_DATAWRITER_CALLBACK_H




