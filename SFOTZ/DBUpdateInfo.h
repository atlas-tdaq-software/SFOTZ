#ifndef SFOTZ_DB_UPDATE_INFO_H
#define SFOTZ_DB_UPDATE_INFO_H

#include <string>
#include <boost/shared_ptr.hpp>

namespace SFOTZ {

struct DBUpdateInfo {

  typedef enum {DEFAULT=2, RUNOPENED, RUNCLOSED, CHECKRUNTRANS,
      LBOPENED, LBCLOSED, CHECKLBTRANS,
      FILEOPENED, FILECLOSED,
      FILETRUNCATED, RUNOPENED_LAX,
      RUNCLOSED_SPECIFIC, LBCLOSED_SPECIFIC} CommandType;

  DBUpdateInfo();
  ~DBUpdateInfo();

  CommandType command;
  std::string lfn;
  std::string pfn;
  unsigned int filenr;
  std::string sfo;
  std::string sfohost;
  unsigned int runnr;
  unsigned int lbnr;
  std::string streamtype;
  std::string stream;
  std::string guid;
  uint64_t filesize;
  std::string checksum;
  unsigned int nrevents;
  std::string sfopfn;
  std::string projecttag;
  unsigned int maxnrsfos;
};

typedef boost::shared_ptr<DBUpdateInfo> DBInfoPointer;

} // SFOTZ namespace

#endif
