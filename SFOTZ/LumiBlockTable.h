/**
 * @file LumiBlockTable.h
 * @author <a href="mailto:Wainer.Vandelli@cern.ch">Wainer Vandelli</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Generic LumiBlockTable description
 */

#ifndef SFOTZ_LUMIBLOCKTABLE_H
#define SFOTZ_LUMIBLOCKTABLE_H

#include "SFOTZ/Table.h"
#include "SFOTZ/IndexTable.h"
#include <vector>
#include <string>

namespace SFOTZ{
  
  /*  namespace LumiBlock{
    enum LumiBlockStates{OPENED, CLOSED, TRANSFERRED};
    }*/

  class LumiBlockTable: public Table{

  public:
    LumiBlockTable(DBConnection & db, const std::string& tablename, 
	     bool createifmissing=false);
    virtual ~LumiBlockTable();

    /**
     * Add a single opened entry to the lumi table. First
     * checks if the entry already exists, in this 
     * case the operation is skipped
     */
    void addOpenedLumiBlockEntry(const std::string& sfo,
				 const unsigned int& runnr,
				 const unsigned int& lbnr,
				 const std::string& streamtype,
				 const std::string& stream,
				 const unsigned int& maxnrsfos,
				 SFOTZ::IndexTable * index,
				 bool transactive=false);
    
     /**
     * Sets a single entry closed if and only if
     * its present state is opened
     */
    void setLumiBlockEntryClosed(const std::string& sfo,
				 const unsigned int& runnr,
				 const unsigned int& lbnr,
				 const std::string& streamtype,
				 const std::string& stream,
				 bool transactive=false);

    /**
     * Sets a single entry transferred if and only if
     * its present state is closed
     */
    void setLumiBlockEntryTransferred(const std::string& sfo,
				      const unsigned int& runnr,
				      const unsigned int& lbnr,
				      const std::string& streamtype,
				      const std::string& stream,
				      bool transactive=false);
     
    /**
     * Sets a all the opened entries to closed state
     */
    void setAllLumiBlockEntryClosed(const std::string& sfo,
				    const unsigned int& runnr,
				    const unsigned int& lbnr,
				    bool transactive=false);
      
     /**
     * Sets a all the closed entries to transferred state
     */
    void setAllLumiBlockEntryTransferred(const std::string& sfo,
					 const unsigned int& runnr,
					 const unsigned int& lbnr,
					 bool transactive=false);


    //Queries
    
    /**
     * Return the list of lumiblocks for which at least one stream 
     * is opened
     */
    std::vector<unsigned int> alreadyOpenedLumiBlocks(const std::string& sfo,
						      const unsigned int& runnr,
						      bool transactive=false);

     /**
     * Return the list of lumiblocks and streams for which at least one stream 
     * is opened
     */
    std::map<unsigned int,
      std::vector<std::pair<std::string, std::string> > > 
      alreadyOpenedLumiBlocksAndStreams(const std::string& sfo,
					const unsigned int& runnr,
					bool transactive=false);


    /**
     * Return the list of lumiblocks for which at all streams
     * are not opened
     */
    std::vector<unsigned int> alreadyNotOpenedLumiBlocks(const std::string& sfo,
							 const unsigned int& runnr,
							 bool transactive=false);
    
    /**
     * Return true is all the entries are closed
     */
    bool allLumiBlockClosed(const std::string& sfo,
			    const unsigned int& runnr,
			    bool transactive=false);
    

    /**
     * Return true is all the entries are transferred
     */
    bool allLumiBlockTransferred(const std::string& sfo,
				 const unsigned int& runnr,
				 const std::string& streamtype,
				 const std::string& stream,
				 bool transactive=false);
       
    /**
     * Return the streams for a given LB
     */
    std::vector<std::pair<std::string,std::string> >
      getStreams(const std::string& sfo,
		 const unsigned int& runnr,
		 const unsigned int& lbnr,
		 bool transactive=false);
		 
  private:
    static coral::TableDescription * 
      createDescription(const std::string& tablename);

    /**
     * Add a single entry to the lumi table. First
     * checks if the entry already exists, in this 
     * case the operation is skipped
     */
    void addNewLumiBlockEntry(const std::string& sfo,
			      const unsigned int& runnr,
			      const unsigned int& lbnr,
			      const std::string& streamtype,
			      const std::string& stream,
			      std::string state,
			      const unsigned int& maxnrsfos,
			      SFOTZ::IndexTable * index,
			      bool transactive=false);
   
     /**
     * Change the entry state.
     * If oldstate is set, the change is done if and only if
     * the present state is as required
     */
    long changeEntryState(const std::string& sfo,
			  const unsigned int& runnr,
			  const unsigned int& lbnr,
			  const std::string& streamtype,
			  const std::string& stream,
			  std::string state,
			  std::string oldstate="",
			  bool transactive=false);

    /**
     * Change all the entry states.
     * If oldstate is set, the change is done if and only if
     * the present state is as required
     */
    void changeAllEntryState(const std::string& sfo,
			     const unsigned int& runnr,
			     const unsigned int& lbnr,
			     std::string state,
			     std::string oldstate="",
			     bool transactive=false);
    
    //Queries
    bool alreadyExist(const std::string& sfo,
		      const unsigned int& runnr,
		      const unsigned int& lbnr,
		      const std::string& streamtype,
		      const std::string& stream,
		      bool transactive=false);
    
  };

}
#endif
