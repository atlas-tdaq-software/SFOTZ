/**
 * @file DBConnection.h
 * @author <a href="mailto:Wainer.Vandelli@cern.ch">Wainer Vandelli</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Open a DB connection and provide a session
 */

#ifndef SFOTO_DBCONNECTION_H
#define SFOTO_DBCONNECTION_H

#include "SFOTZ/DBUpdateInfo.h"

#include "RelationalAccess/ISessionProxy.h"
#include "RelationalAccess/ITransaction.h"
#include "RelationalAccess/ISchema.h"
#include <boost/shared_ptr.hpp>

#include <string>
#include <map>
#include <set>

namespace SFOTZ{
  struct SFOTZCallback;
  
  class DBConnection{
    
  public:
    DBConnection(std::string connection);
    virtual ~DBConnection() {};
    
    coral::ITransaction& getTransaction();
    
    coral::ISchema& getSchema();

  private:
    boost::shared_ptr<coral::ISessionProxy> m_session;

    std::map<DBUpdateInfo::CommandType, std::set<SFOTZCallback*>> m_cb_objects;
    std::vector<DBInfoPointer> m_commands_for_cb;

    /* The real public interface here is SFOTZThread that will un/register callbacks
     * and add processed commands. Also Transaction will trigger the callbacks.
     *
     * But DBConnection is the only class where we can implement it properly.
     * Conceptually we would like to put it in the transaction object (because
     * callbacks are sent at the end of each transaction). But the transaction
     * object is unknown to SFOTZThread. DBConnection is the only place
     * accessible from both SFOTZThread and Transaction.
     *
     * So here it is as private members and SFOTZThread and Transaction are friends.
     */
    friend class SFOTZThread;
    // See SFOTZThread documentation
    void registerCallback(SFOTZCallback*, std::set<DBUpdateInfo::CommandType> const &);
    void unregisterCallback(SFOTZCallback*, std::set<DBUpdateInfo::CommandType> const &);
    void add_command_for_cb(DBInfoPointer);

    friend class Transaction;
    // called by a transaction object just after commit, clears pending commands after send
    void send_commands_for_cb();
    // called by a transaction object just after rollback: clears pending commands
    void clear_commands_for_cb();

    std::set<DBUpdateInfo::CommandType> * all_command_types();
  };

}
#endif
