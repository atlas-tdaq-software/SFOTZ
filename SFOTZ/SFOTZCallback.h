#ifndef SFOTZ_CALLBACK_H
#define SFOTZ_CALLBACK_H

/*! Interface for database interaction callbacks.
 *
 * Implement this interface and register a callback object
 * to a SFOTZThread to be notified when something has been
 * committed to the database.
 */

#include "SFOTZ/DBUpdateInfo.h"

namespace SFOTZ {

struct SFOTZCallback {
  virtual ~SFOTZCallback() {}

  /* Called once for every update on the database after commit. If the same
   * transaction is used to commit several changes at once, each individual
   * change will be notified via this callback right after the commit.
   * The argument is a shared pointer to the original user created object.
   * If a transaction is rolled back for any reason, this callback will
   * never be called.
   */
  virtual void db_updated(DBInfoPointer update_info) = 0;
};

} // SFOTZ namespace

#endif
