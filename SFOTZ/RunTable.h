/**
 * @file RunTable.h
 * @author <a href="mailto:Wainer.Vandelli@cern.ch">Wainer Vandelli</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Generic RunTable description
 */

#ifndef SFOTZ_RUNTABLE_H
#define SFOTZ_RUNTABLE_H

#include "SFOTZ/Table.h"
#include "SFOTZ/IndexTable.h"
#include <vector>
#include <string>

namespace SFOTZ{
  
  //namespace Run{
  // enum States{OPENED, CLOSED, TRANSFERRED};
  //}
 
  class RunTable: public Table{

  public:
    RunTable(DBConnection & db, const std::string& tablename, 
	     bool createifmissing=false);
    virtual ~RunTable();

    /**
     * Add a single opened entry to the run table. First
     * checks if the entry already exists, in this 
     * case the operation is skipped
     */
    void addOpenedRunEntry(const std::string& sfo,
			   const unsigned int& runnr,
			   const std::string& streamtype,
			   const std::string& stream,
			   const std::string& dsname,
			   const std::string& project,
			   const unsigned int& maxnrsfos,
			   SFOTZ::IndexTable * index,
			   bool transactive=false);

    /**
     * Sets a single entry closed if and only if
     * its present state is opened
     */
    void setRunEntryClosed(const std::string& sfo,
			   const unsigned int& runnr,
			   const std::string& streamtype,
			   const std::string& stream,
			   bool transactive=false);

    /**
     * Sets a single entry transferred if and only if
     * its present state is closed
     */
    void setRunEntryTransferred(const std::string& sfo,
				const unsigned int& runnr,
				const std::string& streamtype,
				const std::string& stream,
				bool transactive=false);
    
    /**
     * Sets a all the opened entries to closed state
     */
    void setAllRunEntryClosed(const std::string& sfo,
			      const unsigned int& runnr,
			      bool transactive=false);
      
    /**
     * Sets a all the closed entries to transferred state
     */
    void setAllRunEntryTransferred(const std::string& sfo,
				   const unsigned int& runnr,
				   bool transactive=false);

    //Queries
    /**
     * Returns true if a specific entry already exists
     */
    bool alreadyExist(const std::string& sfo,
		      const unsigned int& runnr,
		      const std::string& streamtype,
		      const std::string& stream,
		      bool transactive=false);
    /**
     * Returns true if at least an entry already exist
     */
    bool entriesAlreadyExist(const std::string& sfo,
			     const unsigned int& runnr,
			     bool transactive=false);

  private:
    static coral::TableDescription * 
      createDescription(const std::string& tablename);
   
    /**
     * Change the entry state.
     * If oldstate is set, the change is done if and only if
     * the present state is as required
     */
    long changeEntryState(const std::string& sfo,
			  const unsigned int& runnr,
			  const std::string& streamtype,
			  const std::string& stream,
			  const std::string state,
			  const std::string oldstate="",
			  bool transactive=false);

    /**
     * Change all the entry states.
     * If oldstate is set, the change is done if and only if
     * the present state is as required
     */
    void changeAllEntryState(const std::string& sfo,
			     const unsigned int& runnr,
			     const std::string state,
			     const std::string oldstate="",
			     bool transactive=false);

    /**
     * Add a single entry to the run table. First
     * checks if the entry already exists, in this 
     * case the operation is skipped
     */
    void addNewRunEntry(const std::string& sfo,
			const unsigned int& runnr,
			const std::string& streamtype,
			const std::string& stream,
			const std::string& state,
			const std::string& dsname,
			const std::string& project,
			const unsigned int& maxnrsfos,
			SFOTZ::IndexTable * index,
			bool transactive=false);

  };

}
#endif
