/**
 * @file IndexTable.h
 * @author <a href="mailto:Wainer.Vandelli@cern.ch">Wainer Vandelli</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Generic IndexTable description
 */

#ifndef SFOTZ_INDEXTABLE_H
#define SFOTZ_INDEXTABLE_H

#include "SFOTZ/Table.h"
#include <vector>
#include <string>

namespace SFOTZ{
  
  namespace Index{
    enum Fields{SEQNAME, SEQINDEX, 
		LASTMOD};
  }
 
  class IndexTable: public Table{

  public:
    IndexTable(DBConnection & db, std::string tablename, 
	     bool createifmissing=false);
    virtual ~IndexTable();

    unsigned int getNextIndex(std::string& seqname, bool transactive=false,
			      unsigned int increment = 1);

  private:
    static coral::TableDescription * createDescription(std::string tablename);
   
  };

}
#endif
