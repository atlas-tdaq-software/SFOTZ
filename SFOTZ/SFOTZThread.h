/**
 * @file SFOTZThread.h
 * @author <a href="mailto:Wainer.Vandelli@cern.ch">Wainer Vandelli</a>
 * @author <a href="mailto:Andreas.Battaglia@cern.ch">Andreas Battaglia</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Database filling thread
 */

#ifndef _SFOTZTHREAD_H
#define _SFOTZTHREAD_H

#include "SFOTZ/IndexTable.h"
#include "SFOTZ/RunTable.h"
#include "SFOTZ/LumiBlockTable.h"
#include "SFOTZ/FileTable.h"
#include "SFOTZ/OverlapTable.h"
#include "SFOTZ/DBUpdateInfo.h"

#include "monsvc/ptr.h"
#include "SFOTZ/info/SFOTZCounters.h"

#include "tbb/concurrent_queue.h"

#include <cstdint>
#include <string>
#include <thread>
#include <atomic>

namespace SFOTZ {

  class SFOTZThread{

  public:
    SFOTZThread(const std::string& connstring,
		const std::string& username,
		const std::string& password,
		const std::string& indextablename,
		const std::string& runtablename,
		const std::string& lumitablename,
		const std::string& filetablename,
		const std::string& overlaptablename="",
		const bool createtables = false,
		const bool queue_monitoring_enabled = false);

    ~SFOTZThread();

    void start();

    void stop();

    // make sure to stop() before calling reset()
    // make sure no new info is add()-ed when calling reset()
    void reset(bool enabled);

    bool empty();

    unsigned int size();

    void add(DBInfoPointer info);

    /*
      IMPORTANT Call the following methods if and
      only if the thread has not been started yet
    */
    bool runAlreadyExists(const std::string& sfoid,
			  const unsigned int& runnr);

    std::vector<std::string>
      staleFiles(const std::string& sfoid,
		 const unsigned int& runnr);

    std::vector<unsigned int>
      staleLBs(const std::string& sfoid,
	       const unsigned int& runnr);

    std::map<unsigned int,
      std::vector<std::pair<std::string, std::string> > >
      staleLBsAndStreams(const std::string& sfoid,
			 const unsigned int& runnr);

    unsigned int
      getMaxIndex(const std::string& sfo,
		  const unsigned int& runnr,
		  const unsigned int& lbnr,
		  const std::string& streamtype,
		  const std::string& stream);

    void publishEoEStats(const std::string& sfo,
			 const unsigned int& runnr,
			 const std::vector<SFOTZ::OverlapData>& streams,
			 const SFOTZ::Overlap::Types& overlaptype);


    static std::string buildConnectionString(const std::string& server,
					     const std::string& name,
					     const std::string& type);

    void publishInfo(const std::string& obj_name, info::SFOTZCounters* info);

    /* Un/register a callback object for the given type of DB update.
     * An empty set of CommandType means un/register to/from everything.
     * See SFOTZCallback for documentation on the callback mechanism.
     * Non-thread safe.
     */
    void registerCallback(SFOTZCallback*, std::set<DBUpdateInfo::CommandType> const &);
    void unregisterCallback(SFOTZCallback*, std::set<DBUpdateInfo::CommandType> const &);

    bool connected() const;

  private:
    void execute();
    void closeLB(DBInfoPointer info);
    void closeRun(DBInfoPointer info);
    void checkLBTransferred(DBInfoPointer info);
    void checkRunTransferred(DBInfoPointer info);

  private:
    bool m_connected;
    bool m_overlap;
    bool m_enabled;
    std::string m_connstring;
    SFOTZ::DBConnection * m_db;
    SFOTZ::IndexTable * m_index_table;
    SFOTZ::RunTable * m_run_table;
    SFOTZ::LumiBlockTable * m_lb_table;
    SFOTZ::FileTable * m_file_table;
    SFOTZ::OverlapTable * m_over_table;
    tbb::concurrent_bounded_queue<DBInfoPointer > m_queue;
    std::atomic<int64_t> m_queue_size; // cannot call m_queue.size() in concurrent context
    std::thread m_thread;
    std::atomic<bool> m_running;
    bool m_queue_monitoring_enabled;
    monsvc::ptr<info::SFOTZCounters> m_is_info;

    static std::atomic<int> s_queue_monitoring_id;
  };

}
#endif
