/**
 * @file Table.h
 * @author <a href="mailto:Wainer.Vandelli@cern.ch">Wainer Vandelli</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Generic table description
 */

#ifndef SFOTO_TABLE_H
#define SFOTO_TABLE_H

#include "RelationalAccess/ITable.h"
#include "RelationalAccess/TableDescription.h"
#include "CoralBase/AttributeSpecification.h"
#include "CoralBase/AttributeList.h"
#include "CoralBase/Attribute.h"
#include "RelationalAccess/IQuery.h"
#include "RelationalAccess/ICursor.h"
#include "RelationalAccess/IColumn.h"

#include "SFOTZ/DBConnection.h"
#include "SFOTZ/Transaction.h"
#include "SFOTZ/SFOTZIssues.h"

#include <string>
#include <ostream>
#include <sstream>
#include <memory>

namespace SFOTZ{
  
  class Table{

  public:
    Table(DBConnection & db, std::string tablename,
	  bool create,
	  coral::TableDescription * description);
    virtual ~Table();

    void resetContent();
    
  protected:
    void insertBuffer(coral::AttributeList& rowBuffer,
		      bool transactive=false);
    long updateEntry(std::string& action, std::string& condition,
		     coral::AttributeList& inputData,
		     bool transactive=false);

    coral::IQuery* newQuery();
    coral::IQuery* newFreeQuery();

    inline std::string equalCondition(char* columnname, 
				      std::string& value){
       return equalCondition(columnname,value.c_str());
    }

    inline std::string equalCondition(char* columnname, 
				      const char* value){
      std::ostringstream oss;
      oss << columnname <<"=:" << value;
      return oss.str();
    }
    
    inline std::string disequalCondition(char* columnname, 
					 std::string& value){
      return disequalCondition(columnname,value.c_str());
    }

    inline std::string disequalCondition(char* columnname, 
					 const char*  value){
      std::ostringstream oss;
      oss << columnname <<"<>:" << value;
      return oss.str();
    }

    inline std::string buildANDCond(std::vector<std::string>& conditions){
      std::ostringstream oss;
      std::vector<std::string>::iterator it = conditions.begin();
      oss << *it;
      it++;
      for(;it!=conditions.end();it++)
	oss << " AND "<< *it;
      return oss.str();
    }

    inline std::string updateAction(char* columnname, 
				    std::string variable){
      std::ostringstream oss;
      oss << columnname <<"=:"<< variable;
      return oss.str();
    }

    inline std::string buildList(std::vector<std::string>& actions){
      std::ostringstream oss;
      std::vector<std::string>::iterator it = actions.begin();
      oss << *it;
      it++;
      for(;it!=actions.end();it++)
	oss << ", "<< *it;
      return oss.str();
    }


  private:
    void createTable(coral::TableDescription& description);
    
  protected:
    SFOTZ::DBConnection & m_db;
    std::string m_table_name;
    coral::TableDescription * m_descr;

  };

}
#endif
