/**
 * @file OverlapTable.h
 * @author <a href="mailto:Wainer.Vandelli@cern.ch">Wainer Vandelli</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Generic OverlapTable description
 */

#ifndef SFOTZ_OVERLAPTABLE_H
#define SFOTZ_OVERLAPTABLE_H

#include "SFOTZ/Table.h"
#include "SFOTZ/IndexTable.h"
#include <vector>
#include <string>

namespace SFOTZ{
  
  namespace Overlap{
    enum Fields{SFOID, RUNNR, STREAM1, STREAM2, OVERLAP, 
		OVERLAP_RATIO, TYPE, TIMESTAMP, OVERLAP_SEQNUM};
    enum Types{EVENTCOUNT, RAW, FILE};
  }
 
  struct OverlapData{
    std::string ref_stream;
    std::string over_stream;
    unsigned int overlap;
    float overlap_ratio;
  };

  class OverlapTable: public Table{

  public:
    OverlapTable(DBConnection & db, const std::string& tablename, 
	     bool createifmissing=false);
    virtual ~OverlapTable();

    
    void addOverlaps(const std::string& sfo,
		     const unsigned int& runnr,
		     const std::vector<SFOTZ::OverlapData> & streams,
		     const SFOTZ::Overlap::Types& overlaptype,
		     SFOTZ::IndexTable * index,
		     bool transactive=false);


  private:
    static coral::TableDescription * 
      createDescription(const std::string& tablename);
   
  };

}
#endif
