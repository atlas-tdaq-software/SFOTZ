/**
 * @file Transaction.h
 * @author <a href="mailto:Wainer.Vandelli@cern.ch">Wainer Vandelli</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Generic transaction description
 */

#ifndef SFOTO_TRANSACTION_H
#define SFOTO_TRANSACTION_H

#include "SFOTZ/DBConnection.h"
#include <string>

namespace SFOTZ{
  
  class Transaction{

  public:
    Transaction(DBConnection & db, bool transactive=false,
		bool readonly=false);
    ~Transaction();

    void commit();

    void rollback();

  private:
    SFOTZ::DBConnection & m_db;
    bool m_open;
    bool m_reuse;
  };

}
#endif
