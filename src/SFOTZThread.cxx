// Dear emacs, this is -*- c++ -*-

/**
 * @file SFOTZThread.cxx
 * @author <a href="mailto:Wainer.vandelli@cern.ch">Wainer Vandelli</a>
 * @author <a href="mailto:Andreas.Battaglia@cern.ch">Andreas Battaglia</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Declares the SFOTZThread.
 */

#include <cstdint>
#include <sstream>
#include <ostream>
#include <stdlib.h>

#include "ers/ers.h"

#include "SFOTZ/SFOTZThread.h"
#include "EventStorage/RawFileName.h"
#include "dal/DBConnection.h"
#include "monsvc/MonitoringService.h"

std::atomic<int> SFOTZ::SFOTZThread::s_queue_monitoring_id(0);

std::string
SFOTZ::SFOTZThread::buildConnectionString(const std::string& server,
					  const std::string& name,
					  const std::string& type){

  std::ostringstream cnn;
  if(type == daq::core::DBConnection::Type::Oracle){
    cnn << "oracle://" << server << "/" << name;
  } else if(type == daq::core::DBConnection::Type::SQLite){
    cnn << "sqlite_file:" << name;
    /*} else if(type=="MySQL"){
      connection = "";
      } else if (type=="DBLookup"){
      connection = "";*/
  } else {
    cnn << "";
  }

  return cnn.str();
}

SFOTZ::SFOTZThread::SFOTZThread(const std::string& connstring,
				const std::string& username,
				const std::string& password,
				const std::string& indextablename,
				const std::string& runtablename,
				const std::string& lumitablename,
				const std::string& filetablename,
				const std::string& overlaptablename,
				const bool createtables,
				const bool queue_monitoring_enabled)
  :m_connected(true),
   m_overlap(true),
   m_enabled(true),
   m_connstring(connstring),
   m_db(0),
   m_index_table(0),
   m_run_table(0),
   m_lb_table(0),
   m_file_table(0),
   m_over_table(0),
   m_queue_size(0),
   m_running(false),
   m_queue_monitoring_enabled(queue_monitoring_enabled)
{

  if(username != "" && password != ""){
    setenv("CORAL_AUTH_USER",
	   username.c_str(),1);
    setenv("CORAL_AUTH_PASSWORD",
	   password.c_str(),1);
    unsetenv("CORAL_AUTH_PATH");
  }

  if(connstring != ""){
    try{
      m_db = new SFOTZ::DBConnection(connstring);
    }catch(SFOTZ::DBConnectionIssue& iss){
      m_connected=false;
      std::ostringstream oss;
      oss << " Cannot create connection. Connection string: " << connstring;
      std::string tmp = oss.str();
      SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), iss);
      ers::error(issue);
    }

    if(m_connected){
      try{
	m_index_table = new SFOTZ::IndexTable(*m_db,
					      indextablename,
					      createtables);
      }catch(SFOTZ::TableIssue& iss){
	m_connected=false;
	std::ostringstream oss;
	oss << " Cannot create table: " << indextablename
	    <<". Connection string: " << connstring;
	std::string tmp = oss.str();
	SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), iss);
	ers::error(issue);
      }

      try{
	m_run_table = new SFOTZ::RunTable(*m_db,
					  runtablename,
					  createtables);
      }catch(SFOTZ::TableIssue& iss){
	m_connected=false;
	std::ostringstream oss;
	oss << " Cannot create table: " << runtablename
	    <<". Connection string: " << connstring;
	std::string tmp = oss.str();
	SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), iss);
	ers::error(issue);
      }

      try{
	m_lb_table = new SFOTZ::LumiBlockTable(*m_db,
					       lumitablename,
					       createtables);
      }catch(SFOTZ::TableIssue& iss){
	m_connected=false;
	std::ostringstream oss;
	oss << " Cannot create table: " << lumitablename
	    <<". Connection string: " << connstring;
	std::string tmp = oss.str();
	SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), iss);
	ers::error(issue);
      }

      try{
	m_file_table = new SFOTZ::FileTable(*m_db,filetablename,
					    createtables);
      }catch(SFOTZ::TableIssue& iss){
	m_connected=false;
	std::ostringstream oss;
	oss << " Cannot create table: " << filetablename
	    <<". Connection string: " << connstring;
	std::string tmp = oss.str();
	SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), iss);
	ers::error(issue);
      }

      if(overlaptablename != ""){
	try{
	  m_over_table = new SFOTZ::OverlapTable(*m_db,overlaptablename,
						 createtables);
	}catch(SFOTZ::TableIssue& iss){
	  m_overlap=false;
	  std::ostringstream oss;
	  oss << " Cannot create table: " << overlaptablename
	      <<". Connection string: " << connstring;
	  std::string tmp = oss.str();
	  SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), iss);
	  ers::warning(issue);
	}
      }else{
	m_overlap = false;
      }
    }

    // IS monitoring
    if (m_queue_monitoring_enabled) {
      // Create unique name for the variable
      std::ostringstream obj_name;
      obj_name << "SFOTZ." << s_queue_monitoring_id++;

      // Try to add the type of db connection (useful for reader)
      std::string::size_type const colon = m_connstring.find(":");
      if (colon != std::string::npos) {
        obj_name << '.' << m_connstring.substr(0, colon);
      }

      std::string const queue_monitoring_objname = obj_name.str();

      static const bool LIB_OWNS_OBJECT = true;

      m_is_info = monsvc::MonitoringService::instance().register_object(
          queue_monitoring_objname, new info::SFOTZCounters, LIB_OWNS_OBJECT,
          std::bind(&SFOTZThread::publishInfo, this,
              std::placeholders::_1, std::placeholders::_2));

      ERS_LOG("SFOTZ queue monitoring enabled for: " << m_connstring
          << " (IS object name: " << queue_monitoring_objname << ")");
    }
  }else{
    m_connected = false;
  }

}


SFOTZ::SFOTZThread::~SFOTZThread(){
  if(m_db){
    delete m_db;
  }

  if(m_index_table){
    delete m_index_table;
  }

  if(m_run_table){
    delete m_run_table;
  }

  if(m_lb_table){
    delete m_lb_table;
  }

  if(m_file_table){
    delete m_file_table;
  }

  if(m_over_table){
    delete m_over_table;
  }

  if (m_queue_monitoring_enabled) {
      monsvc::MonitoringService::instance().remove_object(m_is_info.get());
  }
}

void SFOTZ::SFOTZThread::start(){
  m_thread = std::thread(&SFOTZ::SFOTZThread::execute, this);
}

void SFOTZ::SFOTZThread::stop(){
  m_running = false;
  m_queue.abort();
  m_thread.join();
}

void SFOTZ::SFOTZThread::reset(bool enabled){
  m_enabled = enabled;
  m_queue.clear();
}

bool SFOTZ::SFOTZThread::empty(){
  return m_queue_size <= 0;
}

unsigned int SFOTZ::SFOTZThread::size(){
  int64_t size = m_queue_size;
  if(size < 0){
    size = 0;
  }
  return static_cast<unsigned int>(size);
}

void SFOTZ::SFOTZThread::add(SFOTZ::DBInfoPointer info){
  try{
    m_queue.push(info);
    m_queue_size += 1;
  }catch(tbb::user_abort &e){}

  ERS_DEBUG(2, "Info added into the queue.");
}

bool
SFOTZ::SFOTZThread::runAlreadyExists(const std::string& sfoid,
				     const unsigned int& runnr){
  if(m_connected){
    try{
      return m_run_table->entriesAlreadyExist(sfoid,runnr);
    }catch(SFOTZ::SFOTZIssue& ex){
      std::ostringstream oss;
      oss << " Connection string: " << m_connstring;
      std::string tmp = oss.str();
      SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), ex);
      ers::error(issue);
    }catch(const coral::Exception & ex){
      std::ostringstream oss;
      oss << " Connection string: " << m_connstring;
      std::string tmp = oss.str();
      SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), ex);
      ers::error(issue);
    }
  }
  return false;
}

std::vector<std::string>
SFOTZ::SFOTZThread::staleFiles(const std::string& sfoid,
			       const unsigned int& runnr){

  if(m_connected){
    try{
      return m_file_table->getNotClosedAlreadyExistingFiles(sfoid,
							    runnr);
    }catch(SFOTZ::SFOTZIssue& ex){
      std::ostringstream oss;
      oss << " Connection string: " << m_connstring;
      std::string tmp = oss.str();
      SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), ex);
      ers::error(issue);
    }catch(const coral::Exception & ex){
      std::ostringstream oss;
      oss << " Connection string: " << m_connstring;
      std::string tmp = oss.str();
      SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), ex);
      ers::error(issue);
    }
  }
  std::vector <std::string> empty;
  return empty;
}

std::vector<unsigned int>
SFOTZ::SFOTZThread::staleLBs(const std::string& sfoid,
			     const unsigned int& runnr){
  if(m_connected){
    try{
      return m_lb_table->alreadyOpenedLumiBlocks(sfoid,
						 runnr);
    }catch(SFOTZ::SFOTZIssue& ex){
      std::ostringstream oss;
      oss << " Connection string: " << m_connstring;
      std::string tmp = oss.str();
      SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), ex);
      ers::error(issue);
    }catch(const coral::Exception & ex){
      std::ostringstream oss;
      oss << " Connection string: " << m_connstring;
      std::string tmp = oss.str();
      SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), ex);
      ers::error(issue);
    }
  }
  std::vector <unsigned int> empty;
  return empty;
}

unsigned int
SFOTZ::SFOTZThread::getMaxIndex(const std::string& sfo,
				const unsigned int& runnr,
				const unsigned int& lbnr,
				const std::string& streamtype,
				const std::string& stream){

  if(m_connected){
    try{
      return m_file_table->getMaxIndex(sfo,
				       runnr,
				       lbnr,
				       streamtype,
				       stream);
    }catch(SFOTZ::SFOTZIssue& ex){
      std::ostringstream oss;
      oss << " Connection string: " << m_connstring;
      std::string tmp = oss.str();
      SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), ex);
      ers::error(issue);
    }catch(const coral::Exception & ex){
      std::ostringstream oss;
      oss << " Connection string: " << m_connstring;
      std::string tmp = oss.str();
      SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), ex);
      ers::error(issue);
    }
  }
  return 0;
}


std::map<unsigned int,
	 std::vector<std::pair<std::string, std::string> > >
SFOTZ::SFOTZThread::staleLBsAndStreams(const std::string& sfoid,
				       const unsigned int& runnr){

  if(m_connected){
    try{
      return m_lb_table->alreadyOpenedLumiBlocksAndStreams(sfoid, runnr);
    }catch(SFOTZ::SFOTZIssue& ex){
      std::ostringstream oss;
      oss << " Connection string: " << m_connstring;
      std::string tmp = oss.str();
      SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), ex);
      ers::error(issue);
    }catch(const coral::Exception & ex){
      std::ostringstream oss;
      oss << " Connection string: " << m_connstring;
      std::string tmp = oss.str();
      SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), ex);
      ers::error(issue);
    }
  }
  std::map<unsigned int,
    std::vector<std::pair<std::string, std::string> > > empty;
  return empty;
}


void
SFOTZ::SFOTZThread::publishEoEStats(const std::string& sfo,
				    const unsigned int& runnr,
				    const std::vector<SFOTZ::OverlapData>& streams,
				    const SFOTZ::Overlap::Types& overlaptype){

  if(m_overlap && m_connected){
    try{
      SFOTZ::Transaction tr(*m_db);
      try{
	m_over_table->addOverlaps(sfo,
				  runnr,
				  streams,
				  overlaptype,
				  m_index_table,
				  true);
      }catch(SFOTZ::SFOTZIssue& ex){
	tr.rollback();
	std::ostringstream oss;
	oss << " Connection string: " << m_connstring;
	std::string tmp = oss.str();
	SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), ex);
	ers::error(issue);
      }catch(const coral::Exception & ex){
	tr.rollback();
	std::ostringstream oss;
	oss << " Connection string: " << m_connstring;
	std::string tmp = oss.str();
	SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), ex);
	ers::error(issue);
      }
    }catch(SFOTZ::SFOTZIssue& ex){
      std::ostringstream oss;
      oss << " Connection string: " << m_connstring;
      std::string tmp = oss.str();
      SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), ex);
      ers::error(issue);
    }catch(const coral::Exception & ex){
      std::ostringstream oss;
      oss << " Connection string: " << m_connstring;
      std::string tmp = oss.str();
      SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), ex);
      ers::error(issue);
    }
  }
}

void SFOTZ::SFOTZThread::execute(){
  ERS_DEBUG(2, "SFOTZThread running");
  m_running = true;
  SFOTZ::DBInfoPointer info;

  //Blocks until something is available
  while(m_running) {

    try {
      m_queue.pop(info);
      m_queue_size -= 1;
    } catch(tbb::user_abort &e) {
      break;
    }

    ERS_DEBUG(2, "Got a new DBUpdateInfo");

    if (!m_connected || !m_enabled) {
      continue;
    }

    m_db->add_command_for_cb(info);

    try{
      switch(info->command){
      case SFOTZ::DBUpdateInfo::RUNOPENED_LAX:
        if (m_run_table->alreadyExist(info->sfo,
            info->runnr,
            info->streamtype,
            info->stream)) {
          break;
        }
      case SFOTZ::DBUpdateInfo::RUNOPENED:
      {
        daq::RawFileName rawname(info->projecttag,
            info->runnr,
            info->streamtype,
            info->stream,
            0,"");
        m_run_table->addOpenedRunEntry(info->sfo,
            info->runnr,
            info->streamtype,
            info->stream,
            rawname.datasetName(),
            info->projecttag,
            info->maxnrsfos,
            m_index_table);
      }
      break;
      case SFOTZ::DBUpdateInfo::RUNCLOSED:
        this->closeRun(info);
        break;
      case SFOTZ::DBUpdateInfo::RUNCLOSED_SPECIFIC:
        m_run_table->setRunEntryClosed(info->sfo,
            info->runnr,
            info->streamtype,
            info->stream);
        break;
      case SFOTZ::DBUpdateInfo::CHECKRUNTRANS:
        this->checkRunTransferred(info);
        break;
      case SFOTZ::DBUpdateInfo::LBOPENED:
        m_lb_table->addOpenedLumiBlockEntry(info->sfo,
            info->runnr,
            info->lbnr,
            info->streamtype,
            info->stream,
            info->maxnrsfos,
            m_index_table);
        break;
      case SFOTZ::DBUpdateInfo::LBCLOSED:
        this->closeLB(info);
        break;
      case SFOTZ::DBUpdateInfo::LBCLOSED_SPECIFIC:
        m_lb_table->setLumiBlockEntryClosed(info->sfo,
            info->runnr,
            info->lbnr,
            info->streamtype,
            info->stream);
        break;
      case SFOTZ::DBUpdateInfo::CHECKLBTRANS:
        this->checkLBTransferred(info);
        break;
      case SFOTZ::DBUpdateInfo::FILEOPENED:
        m_file_table->addOpenedFileEntry(info->lfn,
            info->filenr,
            info->sfo,
            info->sfohost,
            info->runnr,
            info->lbnr,
            info->streamtype,
            info->stream,
            info->guid,
            info->sfopfn,
            m_index_table);
        break;
      case SFOTZ::DBUpdateInfo::FILECLOSED:
        m_file_table->setFileEntryClosed(info->lfn,
            info->filenr,
            info->sfo,
            info->runnr,
            info->lbnr,
            info->streamtype,
            info->stream,
            info->filesize,
            info->checksum,
            info->nrevents,
            info->sfopfn);
        break;
      case SFOTZ::DBUpdateInfo::FILETRUNCATED:
        m_file_table->setFileEntryTruncated(info->lfn,
            info->filenr,
            info->sfo,
            info->runnr,
            info->lbnr,
            info->streamtype,
            info->stream,
            info->filesize,
            info->sfopfn);
        break;
      default:
        std::ostringstream oss;
        oss << "Unknown command type: " << info->command;
        std::string tmp = oss.str();
        SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str());
        ers::warning(issue);
        break;
      }
    }
    catch(SFOTZ::SFOTZIssue& ex) {
      std::ostringstream oss;
      oss << " Connection string: " << m_connstring;
      std::string tmp = oss.str();
      SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), ex);
      ers::error(issue);
    }
    catch(const coral::Exception & ex) {
      std::ostringstream oss;
      oss << " Connection string: " << m_connstring;
      std::string tmp = oss.str();
      SFOTZ::RuntimeIssue issue(ERS_HERE, tmp.c_str(), ex);
      ers::error(issue);
    }
  }
}

void
SFOTZ::SFOTZThread::closeLB(SFOTZ::DBInfoPointer info){

  SFOTZ::Transaction tr(*m_db);
  try{
    if(m_file_table->allLBFileClosed(info->sfo,
				     info->runnr,
				     info->lbnr, true)){

      m_lb_table->setAllLumiBlockEntryClosed(info->sfo,
					     info->runnr,
					     info->lbnr,
					     true);
    }else{
      //the files corresponding to this LB are not all closed yet. Let put the request back into the queue
      try{
        m_queue.push(info);
        m_queue_size += 1;
      }catch(tbb::user_abort &e){}
      ERS_DEBUG(1, "Pushing back LB close operation"
		<< " SFO: " << info->sfo
		<< " Run: " << info->runnr
		<< " LB: " << info->lbnr);
    }
  }catch(SFOTZ::SFOTZIssue & ex){
    tr.rollback();
    throw ex;
  }

}

void
SFOTZ::SFOTZThread::closeRun(SFOTZ::DBInfoPointer info){
  SFOTZ::Transaction tr(*m_db);
  try{
    if(m_lb_table->allLumiBlockClosed(info->sfo,
				      info->runnr,
				      true)){

      m_run_table->setAllRunEntryClosed(info->sfo,
					info->runnr,
					true);
    }else{
    //the files corresponding to this LB are not all closed yet. Let put the request back into the queue
      try{
        m_queue.push(info);
        m_queue_size += 1;
      }catch(tbb::user_abort &e){}
      ERS_DEBUG(1, "Pushing back run close operation"
		<< " SFO: " << info->sfo
		<< " Run: " << info->runnr);
    }
  }catch(SFOTZ::SFOTZIssue & ex){
    tr.rollback();
    throw ex;
  }

}

void
SFOTZ::SFOTZThread::checkLBTransferred(SFOTZ::DBInfoPointer info){

  SFOTZ::Transaction tr(*m_db);
  try{
    if(m_file_table->allFileTransferred(info->sfo,
					info->runnr,
					info->lbnr,
					info->streamtype,
					info->stream,
					true)){
      m_lb_table->setLumiBlockEntryTransferred(info->sfo,
					       info->runnr,
					       info->lbnr,
					       info->streamtype,
					       info->stream,
					       true);
    }
  }catch(SFOTZ::SFOTZIssue & ex){
    tr.rollback();
    throw ex;
  }
}

void
SFOTZ::SFOTZThread::checkRunTransferred(SFOTZ::DBInfoPointer info){
  SFOTZ::Transaction tr(*m_db);
  try{
    if(m_lb_table->allLumiBlockTransferred(info->sfo,
					   info->runnr,
					   info->streamtype,
					   info->stream,
					   true)){
      m_run_table->setRunEntryTransferred(info->sfo,
					  info->runnr,
					  info->streamtype,
					  info->stream,
					  true);
    }
  }catch(SFOTZ::SFOTZIssue & ex){
    tr.rollback();
    throw ex;
  }
}

void SFOTZ::SFOTZThread::publishInfo(const std::string& /*obj_name*/, info::SFOTZCounters* info)
{
	info->QueueSize = static_cast<uint32_t>(this->size());
}

void SFOTZ::SFOTZThread::registerCallback(SFOTZ::SFOTZCallback* cb_obj, std::set<SFOTZ::DBUpdateInfo::CommandType> const & command_types) {
  if (m_db)
    m_db->registerCallback(cb_obj, command_types);
}

void SFOTZ::SFOTZThread::unregisterCallback(SFOTZ::SFOTZCallback* cb_obj, std::set<SFOTZ::DBUpdateInfo::CommandType> const & command_types) {
  if (m_db)
    m_db->unregisterCallback(cb_obj, command_types);
}

bool SFOTZ::SFOTZThread::connected() const {
  return m_connected;
}
