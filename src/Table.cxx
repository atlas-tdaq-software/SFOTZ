// Dear emacs, this is -*- c++ -*-

/**
 * @file Table.cxx
 * @author <a href="mailto:Wainer.vandelli@cern.ch">Wainer Vandelli</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Declares the SFOTZ::Table.
 */

#include "SFOTZ/Table.h"
#include "SFOTZ/SFOTZIssues.h"
#include "RelationalAccess/ITableDataEditor.h"
#include "CoralBase/TimeStamp.h"

#include <ostream>

SFOTZ::Table::Table(DBConnection & db, std::string tablename, 
		    bool create, coral::TableDescription * description)
  :m_db(db),
   m_table_name(tablename),
   m_descr(description)
{
  SFOTZ::Transaction tr(m_db);
  if(!m_db.getSchema().existsTable(tablename) && create){
    tr.commit();
    createTable(*description);
  }else if(!m_db.getSchema().existsTable(tablename) && !create){
    tr.rollback();
    std::ostringstream oss;
    oss << "Table " << description->name() << " does not exists";
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str());
    throw issue;
  }    
}

SFOTZ::Table::~Table(){
  delete m_descr;
}

void 
SFOTZ::Table::createTable(coral::TableDescription& description){
   
  SFOTZ::Transaction tr(m_db);
  if(m_db.getSchema().existsTable(description.name())){
    std::ostringstream oss;
    oss << "Table " << description.name() << " already exists";
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str());
    throw issue;
  }else
    m_db.getSchema().createTable(description);
}

void
SFOTZ::Table::insertBuffer(coral::AttributeList& rowBuffer,
			   bool transactive){
  
  SFOTZ::Transaction tr(m_db,transactive);
  try{
    m_db.getSchema().tableHandle(m_table_name).dataEditor().insertRow(rowBuffer);
  } catch (coral::Exception & ex){
    tr.rollback();
    std::ostringstream oss;
    oss << "Cannot insert new row in table: " << m_table_name;
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str(), ex);
    throw issue;
  }
}

void
SFOTZ::Table::resetContent(){
  
  coral::AttributeList conditionData;
  std::string condition = "";
  SFOTZ::Transaction tr(m_db);
  try{
    m_db.getSchema().tableHandle(m_table_name)
      .dataEditor().deleteRows(condition,
			       conditionData);
  } catch (coral::Exception & ex){
    tr.rollback();
    std::ostringstream oss;
    oss << "Cannot reset table: " << m_table_name;
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str(), ex);
    throw issue;
  }
}

long 
SFOTZ::Table::updateEntry(std::string& action, std::string& condition,
			  coral::AttributeList& inputData,
			  bool transactive){
  
  SFOTZ::Transaction tr(m_db,transactive);
  try{
    return m_db.getSchema().tableHandle(m_table_name)
      .dataEditor().updateRows(action, condition, inputData);
    }catch (coral::Exception & ex){
    tr.rollback();
    std::ostringstream oss;
    oss << "Update failed."
	<< " Table: " << m_table_name
	<< " Action: " 	<< action 
	<< " Condition: " << condition;
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str(), ex);
    throw issue;
  }
}

coral::IQuery* 
SFOTZ::Table::newQuery(){
  try{
    return m_db.getSchema().tableHandle(m_table_name).newQuery();
  }catch (coral::Exception & ex){
    std::ostringstream oss;
    oss << "Cannot create a new query for "
	<< " table: " << m_table_name;
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str(), ex);
    throw issue;
  }
}

coral::IQuery* 
SFOTZ::Table::newFreeQuery(){
  try{
    return m_db.getSchema().newQuery();
  }catch (coral::Exception & ex){
    std::ostringstream oss;
    oss << "Cannot create a new query.";
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str(), ex);
    throw issue;
  }
}
