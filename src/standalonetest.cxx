#include <iostream>

#include "RelationalAccess/ConnectionService.h"
#include "CoralKernel/Context.h"
#include "RelationalAccess/IMonitoringService.h"
#include "RelationalAccess/IMonitoringReporter.h"
#include "RelationalAccess/IConnectionServiceConfiguration.h"
#include "RelationalAccess/ISessionProxy.h"
#include "RelationalAccess/ITable.h"
#include "RelationalAccess/TableDescription.h"
#include "RelationalAccess/ISchema.h"
#include "RelationalAccess/ITransaction.h"
#include "CoralBase/AttributeList.h"
#include "CoralBase/Attribute.h"
#include "RelationalAccess/ITableDataEditor.h"
#include "CoralBase/Exception.h"
#include "RelationalAccess/ITypeConverter.h"
#include "RelationalAccess/IColumn.h"
#include "RelationalAccess/IIndex.h"
#include "RelationalAccess/IUniqueConstraint.h"
#include "RelationalAccess/IPrimaryKey.h"
#include "RelationalAccess/IForeignKey.h"
#include "CoralBase/TimeStamp.h"
#include "RelationalAccess/IQuery.h"
#include "RelationalAccess/ICursor.h"
#include <CoralBase/AttributeSpecification.h>
#include <time.h>

void printTableInfo( const coral::ITableDescription& description )\
{
  int numberOfColumns = description.numberOfColumns();
  std::cout << "Table " << description.name();
  std::string tableSpaceName = description.tableSpaceName();
  if ( ! tableSpaceName.empty() )
    std::cout << " (created in tablespace " << tableSpaceName << ")";
  std::cout << " has" << std::endl
            << "  " << numberOfColumns << " columns :" << std::endl;
  for ( int i = 0; i < numberOfColumns; ++i ) {
    const coral::IColumn& column = description.columnDescription( i );
    std::cout << "    " << column.name() << " (" << column.type() << ")";
    if ( column.isUnique() ) std::cout << " UNIQUE";
    if ( column.isNotNull() ) std::cout << " NOT NULL";
    std::cout << std::endl;
  }

  if ( description.hasPrimaryKey() ) {
    const std::vector<std::string>& columnNames = description.primaryKey().columnNames();
    std::cout << "  Primary key defined for column";
    if ( columnNames.size() > 1 ) std::cout << "s";
    std::cout << " ";
    for ( std::vector<std::string>::const_iterator iColumn = columnNames.begin();
          iColumn != columnNames.end(); ++iColumn ) {
      if ( iColumn != columnNames.begin() ) std::cout << ", ";
      std::cout << *iColumn;
    }
    std::string tableSpace = description.primaryKey().tableSpaceName();
    if ( ! tableSpace.empty() )
      std::cout << " in tablespace " << tableSpace;
    std::cout << std::endl;
  }

  int numberOfUniqueConstraints = description.numberOfUniqueConstraints();
  std::cout << "  " << numberOfUniqueConstraints;
  if ( numberOfUniqueConstraints == 1 ) std::cout << " unique constraint:" << std::endl;
  else if ( numberOfUniqueConstraints == 0 ) std::cout << " unique constraints" << std::endl;
  else std::cout << " unique constraints:" << std::endl;
  for ( int i = 0; i < numberOfUniqueConstraints; ++i ) {
    const coral::IUniqueConstraint& uniqueConstraint = description.uniqueConstraint( i );
    std::cout << "    " << uniqueConstraint.name() << " defined for column";
    const std::vector<std::string>& columnNames = uniqueConstraint.columnNames();
    if ( columnNames.size() > 1 ) std::cout << "s";
    std::cout << " ";
    for (  std::vector<std::string>::const_iterator iColumn = columnNames.begin();
           iColumn != columnNames.end(); ++iColumn ) {
      if ( iColumn != columnNames.begin() ) std::cout << ", ";
      std::cout << *iColumn;
    }
    std::string tableSpace = uniqueConstraint.tableSpaceName();
    if ( ! tableSpace.empty() )
      std::cout << " in tablespace " << tableSpace;
    std::cout << std::endl;
  }

  int numberOfIndices = description.numberOfIndices();
  std::cout << "  " << numberOfIndices;
  if ( numberOfIndices == 1 ) std::cout << " index:" << std::endl;
  else if ( numberOfIndices == 0 ) std::cout << " indices" << std::endl;
  else std::cout << " indices:" << std::endl;
  for ( int i = 0; i < numberOfIndices; ++i ) {
    const coral::IIndex& index = description.index( i );
    std::cout << "    " << index.name();
    if ( index.isUnique() ) std::cout << " (UNIQUE)";
    std::cout << " defined for column";
    const std::vector<std::string>& columnNames = index.columnNames();
    if ( columnNames.size() > 1 ) std::cout << "s";
    std::cout << " ";
    for (  std::vector<std::string>::const_iterator iColumn = columnNames.begin();
           iColumn != columnNames.end(); ++iColumn ) {
      if ( iColumn != columnNames.begin() ) std::cout << ", ";
      std::cout << *iColumn;
    }
    std::string tableSpace = index.tableSpaceName();
    if ( ! tableSpace.empty() )
      std::cout << " in tablespace " << tableSpace;
    std::cout << std::endl;
  }

  int numberOfForeignKeys = description.numberOfForeignKeys();
  std::cout << "  " << numberOfForeignKeys;
  if ( numberOfForeignKeys == 1 ) std::cout << " foreign key:" << std::endl;
  else if ( numberOfForeignKeys == 0 ) std::cout << " foreign keys" << std::endl;
  else std::cout << " foreign keys:" << std::endl;
  for ( int i = 0; i < numberOfForeignKeys; ++i ) {
    const coral::IForeignKey& foreignKey = description.foreignKey( i );
    std::cout << "    " << foreignKey.name() << " defined for column";
    const std::vector<std::string>& columnNames = foreignKey.columnNames();
    if ( columnNames.size() > 1 ) std::cout << "s";
    std::cout << " ";
    for (  std::vector<std::string>::const_iterator iColumn = columnNames.begin();
           iColumn != columnNames.end(); ++iColumn ) {
      if ( iColumn != columnNames.begin() ) std::cout << ", ";
      std::cout << *iColumn;
    }
    std::cout << " -> " << foreignKey.referencedTableName() << "( "; 
    const std::vector<std::string>& columnNamesR = foreignKey.referencedColumnNames();
    for (  std::vector<std::string>::const_iterator iColumn = columnNamesR.begin();
           iColumn != columnNamesR.end(); ++iColumn ) {
      if ( iColumn != columnNamesR.begin() ) std::cout << ", ";
      std::cout << *iColumn;
    }
    std::cout << " )" << std::endl;
  }
}

int main(){

  std::string ConnectionName="oracle://atonr_conf/ATLAS_SFO_T0";

  coral::ConnectionService connServ;

  coral::IConnectionServiceConfiguration& conf = connServ.configuration();

  conf.setConnectionRetrialPeriod(10);
  conf.setConnectionRetrialTimeOut(60);
  conf.setConnectionTimeOut(120);

  connServ.setMessageVerbosityLevel(coral::Debug);

  coral::ISessionProxy *proxy = 
    connServ.connect(ConnectionName,coral::Update);
  
  proxy->transaction().start( true );
  
  std::set<std::string> cppTypes = proxy->typeConverter().supportedCppTypes();
  for ( std::set<std::string>::const_iterator iType = cppTypes.begin();
	iType != cppTypes.end(); ++iType ) {
    std::cout << *iType << std::endl;
    std::cout << "                      -> "
	      << proxy->typeConverter().sqlTypeForCppType( *iType ) << std::endl;
  }

  proxy->transaction().commit();

  delete proxy;

  return 0;
}
