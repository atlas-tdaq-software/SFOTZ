/**
 * @file SFODataWriterCallback.cxx
 * @author <a href="mailto:Wainer.Vandelli@cern.ch">Wainer Vandelli</a> 
 * @author <a href="mailto:Andreas.Battaglia@cern.ch">Andreas Battaglia</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Implementation of SFO Callback
 */

#include <iostream>
#include <unistd.h> // for gethostname
#include <string>

#include "SFOTZ/SFOTZDataWriterCallback.h"
#include "ers/ers.h"

#ifndef HOST_NAME_MAX
#define HOST_NAME_MAX 256
#endif


SFOTZ::SFOTZDataWriterCallback::SFOTZDataWriterCallback(std::string appName, 
				    std::vector<SFOTZThread *> dbhandlers)
  :m_appName(appName),
   m_dbhandlers(dbhandlers){
  
  this->Hostname();
}

SFOTZ::SFOTZDataWriterCallback::SFOTZDataWriterCallback(std::string appName, 
				    SFOTZThread * dbhandler)
:m_appName(appName){

  m_dbhandlers.push_back(dbhandler);
  this->Hostname();
}

SFOTZ::SFOTZDataWriterCallback::~SFOTZDataWriterCallback() {}

void SFOTZ::SFOTZDataWriterCallback::Hostname(){
  char buf[HOST_NAME_MAX];
  (::gethostname(buf, HOST_NAME_MAX)< 0) ? m_hostName="UnknownHost": m_hostName=buf;
}

void SFOTZ::SFOTZDataWriterCallback::FileWasOpened(std::string logicalfileName,
					 std::string fileName,
					 std::string streamtype,
					 std::string streamname,
					 std::string sfoid,
					 std::string guid,
					 unsigned int runNumber,
					 unsigned int filenumber,
					 unsigned int lumiblock)
{
  SFOTZ::DBInfoPointer FileOpenStruct(new  DBUpdateInfo());
  
  FileOpenStruct->command = DBUpdateInfo::FILEOPENED;
  FileOpenStruct->lfn = logicalfileName;
  FileOpenStruct->filenr = filenumber;
  FileOpenStruct->sfo = sfoid;
  FileOpenStruct->sfohost = m_hostName;
  FileOpenStruct->runnr = runNumber;
  FileOpenStruct->lbnr = lumiblock;
  FileOpenStruct->streamtype = streamtype;
  FileOpenStruct->stream = streamname;
  FileOpenStruct->guid = guid;
  FileOpenStruct->sfopfn = fileName;

  ERS_DEBUG(1,"File opened callback: " << fileName << ". Informing DBThread.");
  std::vector<SFOTZThread *>::iterator it = m_dbhandlers.begin();
  for(; it != m_dbhandlers.end(); ++it){
    (*it)->add(FileOpenStruct);
  }
}

void SFOTZ::SFOTZDataWriterCallback::FileWasClosed(std::string logicalfileName,
					 std::string fileName,
					 std::string streamtype,
					 std::string streamname,
					 std::string sfoid,
					 std::string guid,
					 std::string checksum,
					 unsigned int eventsInFile,
					 unsigned int runNumber,
					 unsigned int filenumber,
					 unsigned int lumiblock,
					 uint64_t filesize)
{
  
  SFOTZ::DBInfoPointer FileClosedStruct (new DBUpdateInfo());
  
  FileClosedStruct->command = DBUpdateInfo::FILECLOSED;
  FileClosedStruct->lfn = logicalfileName;
  FileClosedStruct->filenr = filenumber;
  FileClosedStruct->sfo = sfoid;
  FileClosedStruct->runnr = runNumber;
  FileClosedStruct->lbnr = lumiblock;
  FileClosedStruct->streamtype = streamtype;
  FileClosedStruct->stream = streamname;
  FileClosedStruct->filesize = filesize;
  FileClosedStruct->checksum = checksum;
  FileClosedStruct->guid = guid;
  FileClosedStruct->nrevents = eventsInFile;
  FileClosedStruct->sfopfn = fileName;

  ERS_DEBUG(1, "File closed callback: " << fileName<< ". Informing DBThread.");
  std::vector<SFOTZThread *>::iterator it = m_dbhandlers.begin();
  for(; it != m_dbhandlers.end(); ++it){
    (*it)->add(FileClosedStruct);
  }
}
