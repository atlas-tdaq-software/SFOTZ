// Dear emacs, this is -*- c++ -*-

/**
 * @file IndexTable.cxx
 * @author <a href="mailto:Wainer.vandelli@cern.ch">Wainer Vandelli</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Declares the SFOTZ::IndexTable.
 */

#include "SFOTZ/IndexTable.h"
#include "SFOTZ/SFOTZIssues.h"
#include "CoralBase/TimeStamp.h"


namespace SFOTZ {
  static char * IndexFieldNames[3]={(char *)"SEQNAME",
				    (char *)"SEQINDEX",
				    (char *)"LASTMOD"};
}

SFOTZ::IndexTable::IndexTable(SFOTZ::DBConnection& db, 
			      std::string tablename,
			      bool createifmissing)
  :Table(db,tablename,createifmissing,createDescription(tablename)){
}

SFOTZ::IndexTable::~IndexTable(){
}


coral::TableDescription *
SFOTZ::IndexTable::createDescription(std::string tablename){

  coral::TableDescription * description = new coral::TableDescription("");
  description->setName(tablename);
  description->insertColumn(IndexFieldNames[Index::SEQNAME],
			    coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 64, false);
  description->insertColumn(IndexFieldNames[Index::SEQINDEX],
			   coral::AttributeSpecification::typeNameForId( typeid(unsigned int) ) );
  description->insertColumn(IndexFieldNames[Index::LASTMOD],
			   coral::AttributeSpecification::typeNameForId( typeid(coral::TimeStamp) ) );
  
  std::vector<std::string> keycomponents;
  keycomponents.push_back(IndexFieldNames[Index::SEQNAME]);
  description->setPrimaryKey(keycomponents);
  
  return description;
}

unsigned int
SFOTZ::IndexTable::getNextIndex(std::string& seqname, 
				bool transactive, unsigned int increment){
    
  SFOTZ::Transaction tr(m_db,transactive);
  std::unique_ptr<coral::IQuery> query(Table::newQuery());
  query->setForUpdate();
  query->addToOutputList(IndexFieldNames[Index::SEQINDEX]);
  query->defineOutputType(IndexFieldNames[Index::SEQINDEX],
			  coral::AttributeSpecification::typeNameForId(typeid (unsigned int)));
  
  std::vector<std::string> conditions;
    
  conditions.push_back(Table::equalCondition(IndexFieldNames[Index::SEQNAME],
					     "seqname"));

  std::string condition = Table::buildANDCond(conditions);

  coral::AttributeList conditionData;
  conditionData.extend<std::string>("seqname");
  conditionData[0].data<std::string>() = seqname;

  query->setCondition( condition, conditionData );

  unsigned int next_id=0;

  try{
    coral::ICursor& cursor = query->execute();
    if(!cursor.next()){
      //We have to create the row
      coral::AttributeList buffer;
      for(int i=0;i<m_descr->numberOfColumns();i++){
	const coral::IColumn &column = m_descr->columnDescription(i);
	buffer.extend (column.name(), column.type());
      }
          
      buffer[Index::SEQNAME].data<std::string>() = seqname;
      buffer[Index::SEQINDEX].data<unsigned int>() = next_id+increment;
      buffer[Index::LASTMOD].data<coral::TimeStamp>() = coral::TimeStamp::now();
      Table::insertBuffer(buffer,true);

    }else{
      //The row is there. Fetch the value and update
      next_id = cursor.currentRow()[0].data<unsigned int>();
      
      std::vector<std::string> actions;
      actions.push_back(Table::updateAction(IndexFieldNames[Index::SEQINDEX],
					    "newindex"));
      actions.push_back(Table::updateAction(IndexFieldNames[Index::LASTMOD], 
					    "newtime"));
      
      std::string updateAction = Table::buildList(actions);
      
      std::vector<std::string> conditions;
      conditions.push_back(Table::equalCondition(IndexFieldNames[Index::SEQNAME],
						 "seqname"));
          
      std::string updateCondition = Table::buildANDCond(conditions);
        
      coral::AttributeList updateData;
      updateData.extend<unsigned int>("newindex");
      updateData[0].data<unsigned int>() = next_id+increment;
      updateData.extend<coral::TimeStamp>("newtime");
      updateData[1].data<coral::TimeStamp>() = coral::TimeStamp::now();
      updateData.extend<std::string>("seqname");
      updateData[2].data<std::string>() = seqname;
      
      Table::updateEntry(updateAction,updateCondition,
			 updateData,true);
      
    }
  } catch(coral::Exception & ex){
    tr.rollback();
    std::ostringstream oss;
    oss << "Fetching or update of the index failed. Sequence: " << seqname;
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str(), ex);
    throw issue;
  }

  return next_id;
}
