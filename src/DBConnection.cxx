// Dear emacs, this is -*- c++ -*-

/**
 * @file DBConnection.cxx
 * @author <a href="mailto:Wainer.vandelli@cern.ch">Wainer Vandelli</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Declares the SFOTZ::DBConnection.
 */

#include "RelationalAccess/ConnectionService.h"
#include "RelationalAccess/IConnectionServiceConfiguration.h"
#include "RelationalAccess/ITransaction.h"
#include "CoralBase/MessageStream.h"

#include "SFOTZ/DBConnection.h"
#include "SFOTZ/SFOTZIssues.h"
#include "SFOTZ/DBUpdateInfo.h"
#include "SFOTZ/SFOTZCallback.h"

#include <ostream>
#include <sstream>

#include <stdlib.h>

SFOTZ::DBConnection::DBConnection(std::string connection)
  :m_session(){

  ERS_DEBUG(1,"Opening a new connection: " << connection.c_str());

  coral::ConnectionService connServ;

  coral::IConnectionServiceConfiguration& conf = connServ.configuration();

  conf.setConnectionRetrialPeriod(10);
  conf.setConnectionRetrialTimeOut(60);
  conf.setConnectionTimeOut(120);
  
  if(getenv("SFO_DEBUG_CORAL") != NULL){
    connServ.setMessageVerbosityLevel(coral::Debug);
  }else{
    connServ.setMessageVerbosityLevel(coral::Warning);
  }

  try{
    m_session.reset(connServ.connect(connection, coral::Update)); 
  }catch(const coral::Exception & exc){
    std::ostringstream oss;
    oss << "Cannot open DB connection. Connection string: " << connection;
    std::string tmp = oss.str();
    SFOTZ::DBConnectionIssue issue(ERS_HERE, tmp.c_str(), exc);
    throw issue;
  }
}

coral::ITransaction& SFOTZ::DBConnection::getTransaction(){
  return m_session->transaction();
}

coral::ISchema& SFOTZ::DBConnection::getSchema(){
  return m_session->nominalSchema();
}

void SFOTZ::DBConnection::registerCallback(SFOTZCallback* cb_ptr, std::set<DBUpdateInfo::CommandType> const & command_types)
{
  std::set<DBUpdateInfo::CommandType> const * cmd_types = &command_types;

  if (command_types.empty()) {
    cmd_types = all_command_types();
  }

  for (auto const cmd_type : *cmd_types) {
    m_cb_objects[cmd_type].insert(cb_ptr);
  }
}

void SFOTZ::DBConnection::unregisterCallback(SFOTZCallback* cb_ptr, std::set<DBUpdateInfo::CommandType> const & command_types)
{
  std::set<DBUpdateInfo::CommandType> const * cmd_types = &command_types;

  if (command_types.empty()) {
    cmd_types = all_command_types();
  }

  for (auto const cmd_type : *cmd_types) {
    m_cb_objects[cmd_type].erase(cb_ptr);
  }
}

void SFOTZ::DBConnection::add_command_for_cb(DBInfoPointer command)
{
  m_commands_for_cb.push_back(command);
}

void SFOTZ::DBConnection::send_commands_for_cb()
{
  for (DBInfoPointer cmd : m_commands_for_cb) {
    for (SFOTZCallback* cb_obj : m_cb_objects[cmd->command]) {
      cb_obj->db_updated(cmd);
    }
  }
  m_commands_for_cb.clear();
}

void SFOTZ::DBConnection::clear_commands_for_cb()
{
  m_commands_for_cb.clear();
}

std::set<SFOTZ::DBUpdateInfo::CommandType> * SFOTZ::DBConnection::all_command_types()
{
  static std::set<DBUpdateInfo::CommandType> all_command_types;
  if (all_command_types.empty()) {
    all_command_types.insert(DBUpdateInfo::DEFAULT);
    all_command_types.insert(DBUpdateInfo::RUNOPENED);
    all_command_types.insert(DBUpdateInfo::RUNCLOSED);
    all_command_types.insert(DBUpdateInfo::CHECKRUNTRANS);
    all_command_types.insert(DBUpdateInfo::LBOPENED);
    all_command_types.insert(DBUpdateInfo::LBCLOSED);
    all_command_types.insert(DBUpdateInfo::CHECKLBTRANS);
    all_command_types.insert(DBUpdateInfo::FILEOPENED);
    all_command_types.insert(DBUpdateInfo::FILECLOSED);
    all_command_types.insert(DBUpdateInfo::FILETRUNCATED);
    all_command_types.insert(DBUpdateInfo::RUNOPENED_LAX);
    all_command_types.insert(DBUpdateInfo::RUNCLOSED_SPECIFIC);
    all_command_types.insert(DBUpdateInfo::LBCLOSED_SPECIFIC);
  }
  return &all_command_types;
}
