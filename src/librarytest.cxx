
#include "SFOTZ/DBConnection.h"
#include "SFOTZ/RunTable.h"
#include "SFOTZ/IndexTable.h"
#include "SFOTZ/LumiBlockTable.h"
#include "SFOTZ/FileTable.h"
#include "SFOTZ/OverlapTable.h"
#include "SFOTZ/SFOTZIssues.h"
#include <iostream>
#include <sstream>

int main(){

  //SFOTZ::DBConnection * db =  new SFOTZ::DBConnection("sqlite_file:test.db");
  SFOTZ::DBConnection * db =  new SFOTZ::DBConnection("oracle://intr/ATLAS_SFO_T0");

  SFOTZ::RunTable * run;
  try{
    run = new SFOTZ::RunTable(*db,"TEST_RUN",true);
    ERS_INFO("run created");
  }catch(SFOTZ::TableIssue & ex){
    std::cout << ex.what() << std::endl;
    return -1;
  }

  SFOTZ::LumiBlockTable * lb;
  try{
    lb = new SFOTZ::LumiBlockTable(*db,"TEST_LUMI",true);
    ERS_INFO("lumi created");
  }catch(SFOTZ::TableIssue & ex){
    std::cout << ex.what() << std::endl;
    return -1;
  }
  
  SFOTZ::FileTable * file;
  try{
    file = new SFOTZ::FileTable(*db,"TEST_FILE",true);
    ERS_INFO("file created");
  }catch(SFOTZ::TableIssue & ex){
    std::cout << ex.what() << std::endl;
    return -1;
  }
  
  SFOTZ::IndexTable * index;
  try{
    index = new SFOTZ::IndexTable(*db,"TEST_SEQINDEX",true);
    ERS_INFO("Index created");
  }catch(SFOTZ::TableIssue & ex){
    std::cout << ex.what() << std::endl;
    return -1;
  }

  SFOTZ::OverlapTable * overlap;
  try{
    overlap = new SFOTZ::OverlapTable(*db,"TEST_OVERLAP",true);
    ERS_INFO("Overlap created");
  }catch(SFOTZ::TableIssue & ex){
    std::cout << ex.what() << std::endl;
    return -1;
  }

  std::string run_index("TEST_RUN");
  index->getNextIndex(run_index);

  std::string lumi_index("TEST_LUMI");
  index->getNextIndex(lumi_index);

  std::string file_index("TEST_FILE");
  index->getNextIndex(file_index);

  unsigned int base = 20;

  for(unsigned int i=1; i<=10; i++){
    
    std::string app("SFO-1");
    std::string streamtype("Physics");
    std::string streamname("Muons");
    std::string tag("");
    run->addOpenedRunEntry(app, base+i, 
			   streamtype, streamname, tag, tag,
                           1,
			   index);
    
    
    lb->addOpenedLumiBlockEntry(app, base+i, 1,
                                streamtype, streamname, 1, index);
    
    std::ostringstream oss;
    oss << (base+i);

    std::string host("pc-tdq-sfo-01.cern.ch");
    std::string guid("guid");
    std::string lfn("filename1.data.writing");
    std::string pfn("/raid1/data/filename1.data.writing");
    file->addOpenedFileEntry(lfn + oss.str(),
			     1,
			     app,
			     host,
			     base+i,
			     1,
			     streamtype,
			     streamname,
			     guid,
			     pfn + oss.str(),
			     index);
    
   }

  std::vector<SFOTZ::OverlapData> datav;

  SFOTZ::OverlapData data;
  
  data.ref_stream = "stream_A";
  data.over_stream = "stream_A";
  data.overlap = 100;
  data.overlap_ratio = 1.0;

  datav.push_back(data);

  data.ref_stream = "stream_B";
  data.over_stream = "stream_B";
  data.overlap = 200;
  data.overlap_ratio = 1.0;

  datav.push_back(data);

  data.ref_stream = "stream_A";
  data.over_stream = "stream_B";
  data.overlap = 50;
  data.overlap_ratio = 0.5;

  datav.push_back(data);

  data.ref_stream = "stream_B";
  data.over_stream = "stream_A";
  data.overlap = 50;
  data.overlap_ratio = 0.25;

  datav.push_back(data);
 

  SFOTZ::Overlap::Types type = SFOTZ::Overlap::RAW;
  std::string app = "SFO-1";
  unsigned int runnr = 1233;

  overlap->addOverlaps(app,
		       runnr,
		       datav,
		       type,
		       index);

  delete run;
  
  delete lb;
  
  delete file;
  
  delete index;

  delete overlap;

  std::cout << "Going to delete db" << std::endl;

  delete db;

  std::cout << "Going to exit" << std::endl;
  return 0;
}
