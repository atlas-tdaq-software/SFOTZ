// Dear emacs, this is -*- c++ -*-

/**
 * @file LumiBlockTable.cxx
 * @author <a href="mailto:Wainer.vandelli@cern.ch">Wainer Vandelli</a>
 * $Author$
 * $Revision$
 * $Date$
 * $Revision$
 * $Date$
 *
 * @brief Declares the SFOTZ::LumiBlockTable.
 */

#include "SFOTZ/LumiBlockTable.h"
#include "SFOTZ/SFOTZIssues.h"
#include "CoralBase/TimeStamp.h"

namespace SFOTZ {
  static char * LumiBlockStateNames[3]={(char *)"OPENED", (char *)"CLOSED", 
					(char *)"TRANSFERRED"};
  
  static char * LumiBlockFieldNames[10]={(char *)"SFOID", (char *)"RUNNR", 
					 (char *)"LUMIBLOCKNR",
					 (char *)"STREAMTYPE", 
					 (char *)"STREAM", 
					 (char *)"STATE", (char *)"TZSTATE",
					 (char *)"T_STAMP",
					 (char *)"LUMIBLOCK_SEQNUM",
					 (char *) "MAXNRSFOS"};

  namespace LumiBlock{
    enum LumiBlockFields{SFOID, RUNNR, LUMIBLOCKNR, 
			 STREAMTYPE, STREAM, STATE, TZSTATE, T_STAMP,
			 LUMIBLOCK_SEQNUM, MAXNRSFOS};
    enum LumiBlockStates{OPENED, CLOSED, TRANSFERRED};
  }
}

SFOTZ::LumiBlockTable::LumiBlockTable(SFOTZ::DBConnection& db, 
				      const std::string& tablename,
				      bool createifmissing)
  :Table(db,tablename,createifmissing,createDescription(tablename)){
}

SFOTZ::LumiBlockTable::~LumiBlockTable(){
}


coral::TableDescription *
SFOTZ::LumiBlockTable::createDescription(const std::string& tablename){

  coral::TableDescription * description = new coral::TableDescription("");
  description->setName(tablename);
  description->insertColumn(LumiBlockFieldNames[LumiBlock::SFOID],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 32, false );
  description->insertColumn(LumiBlockFieldNames[LumiBlock::RUNNR],
			   coral::AttributeSpecification::typeNameForId( typeid(unsigned int) ) );
  description->insertColumn(LumiBlockFieldNames[LumiBlock::LUMIBLOCKNR],
			   coral::AttributeSpecification::typeNameForId( typeid(unsigned int) ) );
  description->insertColumn(LumiBlockFieldNames[LumiBlock::STREAMTYPE],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 32, false );
  description->insertColumn(LumiBlockFieldNames[LumiBlock::STREAM],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 32, false );
  description->insertColumn(LumiBlockFieldNames[LumiBlock::STATE],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 32, false );
  description->insertColumn(LumiBlockFieldNames[LumiBlock::TZSTATE],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 32, false );
  description->insertColumn(LumiBlockFieldNames[LumiBlock::T_STAMP],
			   coral::AttributeSpecification::typeNameForId( typeid(coral::TimeStamp) ) );
  description->insertColumn(LumiBlockFieldNames[LumiBlock::LUMIBLOCK_SEQNUM],
			    coral::AttributeSpecification::typeNameForId( typeid(unsigned int) ) );
  description->insertColumn(LumiBlockFieldNames[LumiBlock::MAXNRSFOS],
			    coral::AttributeSpecification::typeNameForId( typeid(unsigned int) ) );
  
  std::vector<std::string> keycomponents;
  keycomponents.push_back(LumiBlockFieldNames[LumiBlock::SFOID]);
  keycomponents.push_back(LumiBlockFieldNames[LumiBlock::RUNNR]);
  keycomponents.push_back(LumiBlockFieldNames[LumiBlock::LUMIBLOCKNR]);
  keycomponents.push_back(LumiBlockFieldNames[LumiBlock::STREAMTYPE]);
  keycomponents.push_back(LumiBlockFieldNames[LumiBlock::STREAM]);
  
  description->setPrimaryKey(keycomponents);

  description->createIndex(tablename+"_SEQINDEX",
			   LumiBlockFieldNames[LumiBlock::LUMIBLOCK_SEQNUM],
			   true);
  return description;
}

bool 
SFOTZ::LumiBlockTable::alreadyExist(const std::string& sfo,
				    const unsigned int& runnr,
				    const unsigned int& lbnr,
				    const std::string& streamtype,
				    const std::string& stream,
				    bool transactive){
  
  SFOTZ::Transaction tr(m_db,transactive);
  std::unique_ptr<coral::IQuery> query(Table::newQuery());
  query->addToOutputList(LumiBlockFieldNames[LumiBlock::SFOID]);
  query->defineOutputType(LumiBlockFieldNames[LumiBlock::SFOID],
			  coral::AttributeSpecification::
			  typeNameForId(typeid(std::string)));
  
  std::vector<std::string> conditions;

  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::SFOID],
				      "sfo"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::RUNNR],
				      "runnr"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::
							  LUMIBLOCKNR],
				      "lbnr"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::
							  STREAMTYPE],
				      "streamtype"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::STREAM],
				      "stream"));

  std::string condition = Table::buildANDCond(conditions);

  coral::AttributeList conditionData;
  conditionData.extend<std::string>("sfo");
  conditionData[0].data<std::string>() = sfo;
  conditionData.extend<unsigned int>("runnr");
  conditionData[1].data<unsigned int>() = runnr;
  conditionData.extend<unsigned int>("lbnr");
  conditionData[2].data<unsigned int>() = lbnr;
  conditionData.extend<std::string>("streamtype");
  conditionData[3].data<std::string>() = streamtype;
  conditionData.extend<std::string>("stream");
  conditionData[4].data<std::string>() = stream;

  query->setCondition( condition, conditionData );
  try{
    coral::ICursor& cursor = query->execute();
    return cursor.next();
  } catch(coral::Exception & ex){
    tr.rollback();
    std::ostringstream oss;
    oss << "Check for existing LUMI entries table " 
	<< m_table_name << " failed.";
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str(), ex);
    throw issue;
  }
}

void 
SFOTZ::LumiBlockTable::addNewLumiBlockEntry(const std::string& sfo,
					    const unsigned int& runnr,
					    const unsigned int& lbnr,
					    const std::string& streamtype,
					    const std::string& stream,
					    std::string state,
					    const unsigned int& maxnrsfos,
					    SFOTZ::IndexTable * index,
					    bool transactive){
  
  if(!alreadyExist(sfo,runnr,lbnr,streamtype,stream,transactive)){
    coral::AttributeList buffer;
        
    for(int i=0;i<m_descr->numberOfColumns();i++){
      const coral::IColumn &column = m_descr->columnDescription(i);
      buffer.extend (column.name(), column.type());
    }

    std::string colname(LumiBlockFieldNames[LumiBlock::LUMIBLOCK_SEQNUM]);
    unsigned int next_index = index->
      getNextIndex(colname, transactive);

    buffer[LumiBlock::SFOID].data<std::string>() = sfo;
    buffer[LumiBlock::RUNNR].data<unsigned int>() = runnr;
    buffer[LumiBlock::LUMIBLOCKNR].data<unsigned int>() = lbnr;
    buffer[LumiBlock::STREAMTYPE].data<std::string>() = streamtype;
    buffer[LumiBlock::STREAM].data<std::string>() = stream;
    buffer[LumiBlock::STATE].data<std::string>() = state;
    buffer[LumiBlock::TZSTATE].data<std::string>() = "NONE";
    buffer[LumiBlock::T_STAMP].data<coral::TimeStamp>() = 
      coral::TimeStamp::now();
    buffer[LumiBlock::LUMIBLOCK_SEQNUM].data<unsigned int>() = next_index;
    buffer[LumiBlock::MAXNRSFOS].data<unsigned int>() = maxnrsfos;
    
    Table::insertBuffer(buffer,transactive);
  }else{
    std::ostringstream oss;
    oss << "Insert skipped, entry already exists. Table: " << m_table_name
	<< " SFOID=" << sfo
	<< " RUNNR=" << runnr
	<< " LUMIBLOCKNR=" << lbnr
	<< " STREAMTYPE=" << streamtype
	<< " STREAM="<< stream;
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str());
    ers::warning(issue);
  }
}

void 
SFOTZ::LumiBlockTable::addOpenedLumiBlockEntry(const std::string& sfo,
					       const unsigned int& runnr,
					       const unsigned int& lbnr,
					       const std::string& streamtype,
					       const std::string& stream,
					       const unsigned int& maxnrsfos,
					       SFOTZ::IndexTable * index,
					       bool transactive){
  addNewLumiBlockEntry(sfo, runnr, lbnr,
		       streamtype, stream,
		       std::string(LumiBlockStateNames[LumiBlock::OPENED]),
		       maxnrsfos,
		       index,
		       transactive);
 }


long 
SFOTZ::LumiBlockTable::changeEntryState(const std::string& sfo,
					const unsigned int& runnr,
					const unsigned int& lbnr,
					const std::string& streamtype,
					const std::string& stream,
					std::string state,
					std::string oldstate,
					bool transactive){
   
  std::vector<std::string> actions;
  actions.push_back(Table::updateAction(LumiBlockFieldNames[LumiBlock::STATE],
					"newstate"));
  actions.push_back(Table::updateAction(LumiBlockFieldNames[LumiBlock::T_STAMP], 
					"newtime"));

  std::string updateAction = Table::buildList(actions);

  std::vector<std::string> conditions;
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::SFOID],
				      "sfo"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::RUNNR],
				      "runnr"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::
							  LUMIBLOCKNR],
				      "lbnr"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::
							  STREAMTYPE],
				      "streamtype"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::STREAM],
				      "stream"));
  if(oldstate!=""){
    conditions.push_back(Table::
			 equalCondition(LumiBlockFieldNames[LumiBlock::STATE],
					"oldstate"));
  }
    
  std::string updateCondition = Table::buildANDCond(conditions);
    
  coral::AttributeList updateData;
  updateData.extend<std::string>("newstate");
  updateData[0].data<std::string>() = state;
  updateData.extend<coral::TimeStamp>("newtime");
  updateData[1].data<coral::TimeStamp>() = coral::TimeStamp::now();
  updateData.extend<std::string>("sfo");
  updateData[2].data<std::string>() = sfo;
  updateData.extend<unsigned int>("runnr");
  updateData[3].data<unsigned int>() = runnr;
  updateData.extend<unsigned int>("lbnr");
  updateData[4].data<unsigned int>() = lbnr;
  updateData.extend<std::string>("streamtype");
  updateData[5].data<std::string>() = streamtype;
  updateData.extend<std::string>("stream");
  updateData[6].data<std::string>() = stream;
  if(oldstate!=""){
      updateData.extend<std::string>("oldstate");
      updateData[7].data<std::string>() = oldstate;
  }
  
    
  return Table::updateEntry(updateAction,updateCondition,
			    updateData,transactive);
  
}

void 
SFOTZ::LumiBlockTable::setLumiBlockEntryClosed(const std::string& sfo,
					       const unsigned int& runnr,
					       const unsigned int& lbnr,
					       const std::string& streamtype,
					       const std::string& stream,
					       bool transactive){
  
  long nrows = changeEntryState(sfo, runnr, lbnr,
				streamtype, stream,
				LumiBlockStateNames[LumiBlock::CLOSED],
				LumiBlockStateNames[LumiBlock::OPENED],
				transactive);
  if(nrows!=1){
    std::ostringstream oss;
    oss << "Update action on table " << m_table_name 
	<< " affected " 
	<< nrows << " rows " 
	<< " instead of 1."
	<< " Action: Transferred "
	<< " SFO: " << sfo
	<< " RUN: " << runnr
	<< " LBNR: " << lbnr
	<< " Streamtype: " << streamtype
	<< " Streamname: " << stream;
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str());
    throw issue;
  }
}

void 
SFOTZ::LumiBlockTable::setLumiBlockEntryTransferred(const std::string& sfo,
						    const unsigned int& runnr,
						    const unsigned int& lbnr,
						    const std::string& streamtype,
						    const std::string& stream,
						    bool transactive){

  long nrows = changeEntryState(sfo, runnr, lbnr,
				streamtype, stream,
				LumiBlockStateNames[LumiBlock::TRANSFERRED],
				LumiBlockStateNames[LumiBlock::CLOSED],
				transactive);

  if(nrows!=1){
    std::ostringstream oss;
    oss << "Update action on table " << m_table_name 
	<< " affected " 
	<< nrows << " rows " 
	<< " instead of 1."
	<< " Action: Transferred "
	<< " SFO: " << sfo
	<< " RUN: " << runnr
	<< " LBNR: " << lbnr
	<< " Streamtype: " << streamtype
	<< " Streamname: " << stream;
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str());
    throw issue;
  }
}
        

void 
SFOTZ::LumiBlockTable::changeAllEntryState(const std::string& sfo,
					   const unsigned int& runnr,
					   const unsigned int& lbnr,
					   std::string state,
					   std::string oldstate,
					   bool transactive){

  std::vector<std::string> actions;
  actions.push_back(Table::updateAction(LumiBlockFieldNames[LumiBlock::STATE],
					"newstate"));
  actions.push_back(Table::
		    updateAction(LumiBlockFieldNames[LumiBlock::T_STAMP], 
				 "newtime"));

  std::string updateAction = Table::buildList(actions);

  std::vector<std::string> conditions;
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::SFOID],
				      "sfo"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::RUNNR],
				      "runnr"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::
							  LUMIBLOCKNR],
				      "lbnr"));
  if(oldstate!=""){
    conditions.push_back(Table::
			 equalCondition(LumiBlockFieldNames[LumiBlock::STATE],
					"oldstate"));
  }


  std::string updateCondition = Table::buildANDCond(conditions);
    
  coral::AttributeList updateData;
  updateData.extend<std::string>("newstate");
  updateData[0].data<std::string>() = state;
  updateData.extend<coral::TimeStamp>("newtime");
  updateData[1].data<coral::TimeStamp>() = coral::TimeStamp::now();
  updateData.extend<std::string>("sfo");
  updateData[2].data<std::string>() = sfo;
  updateData.extend<unsigned int>("runnr");
  updateData[3].data<unsigned int>() = runnr;
  updateData.extend<unsigned int>("lbnr");
  updateData[4].data<unsigned int>() = lbnr;
  if(oldstate!=""){
      updateData.extend<std::string>("oldstate");
      updateData[5].data<std::string>() = oldstate;
  }
  
  Table::updateEntry(updateAction,updateCondition,
		     updateData,transactive);
}

void 
SFOTZ::LumiBlockTable::setAllLumiBlockEntryClosed(const std::string& sfo,
						  const unsigned int& runnr,
						  const unsigned int& lbnr,
						  bool transactive){

  changeAllEntryState(sfo, runnr, lbnr,
		      LumiBlockStateNames[LumiBlock::CLOSED],
		      LumiBlockStateNames[LumiBlock::OPENED],
		      transactive);
}


void 
SFOTZ::LumiBlockTable::setAllLumiBlockEntryTransferred(const std::string& sfo,
						       const unsigned int& runnr,
						       const unsigned int& lbnr,
						       bool transactive){
  changeAllEntryState(sfo, runnr, lbnr,
		      LumiBlockStateNames[LumiBlock::TRANSFERRED],
		      LumiBlockStateNames[LumiBlock::CLOSED],
		      transactive);
}


std::vector<unsigned int> 
SFOTZ::LumiBlockTable::alreadyOpenedLumiBlocks(const std::string& sfo,
					       const unsigned int& runnr,
					       bool transactive){

  SFOTZ::Transaction tr(m_db,transactive);
  std::vector<unsigned int> lbs;
  std::unique_ptr<coral::IQuery> query(Table::newQuery());
  query->addToOutputList(LumiBlockFieldNames[LumiBlock::LUMIBLOCKNR]);
  query->defineOutputType(LumiBlockFieldNames[LumiBlock::LUMIBLOCKNR],
			  coral::AttributeSpecification::
			  typeNameForId(typeid(unsigned int)));
  query->addToOrderList(LumiBlockFieldNames[LumiBlock::LUMIBLOCKNR]);
  
  std::vector<std::string> conditions;

  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::SFOID],
				      "sfo"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::RUNNR],
				      "runnr"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::STATE],
				      "state"));
  
  std::string condition = Table::buildANDCond(conditions);
  
  coral::AttributeList updateData;
  updateData.extend<std::string>("sfo");
  updateData[0].data<std::string>() = sfo;
  updateData.extend<unsigned int>("runnr");
  updateData[1].data<unsigned int>() = runnr;
  std::string state(LumiBlockStateNames[LumiBlock::OPENED]);
  updateData.extend<std::string>("state");
  updateData[2].data<std::string>() = state;
  
  query->setCondition( condition, updateData );
  query->setDistinct();   	
  try{
    coral::ICursor& cursor = query->execute();
    
    while(cursor.next()){
      lbs.push_back(cursor.currentRow()[LumiBlockFieldNames[LumiBlock::LUMIBLOCKNR]].data<unsigned int>());
    }
  
  } catch(coral::Exception & ex){
    tr.rollback();
    std::ostringstream oss;
    oss << "Search for opened LUMI entries in table " 
	<< m_table_name << " failed.";
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str(), ex);
    throw issue;
  }
  return lbs;
}

std::map<unsigned int,
	 std::vector<std::pair<std::string, std::string> > > 
SFOTZ::LumiBlockTable::alreadyOpenedLumiBlocksAndStreams(const std::string& sfo,
							 const unsigned int& runnr,
							 bool transactive){

  std::map<unsigned int,
    std::vector<std::pair<std::string, std::string> > > lbs;
  SFOTZ::Transaction tr(m_db,transactive);
  std::unique_ptr<coral::IQuery> query(Table::newQuery());
  query->addToOutputList(LumiBlockFieldNames[LumiBlock::LUMIBLOCKNR]);
  query->defineOutputType(LumiBlockFieldNames[LumiBlock::LUMIBLOCKNR],
			  coral::AttributeSpecification::
			  typeNameForId(typeid(unsigned int)));
  query->addToOrderList(LumiBlockFieldNames[LumiBlock::LUMIBLOCKNR]);
  query->addToOutputList(LumiBlockFieldNames[LumiBlock::STREAMTYPE]);
  query->defineOutputType(LumiBlockFieldNames[LumiBlock::STREAMTYPE],
			  coral::AttributeSpecification::
			  typeNameForId(typeid(std::string)));
  query->addToOutputList(LumiBlockFieldNames[LumiBlock::STREAM]);
  query->defineOutputType(LumiBlockFieldNames[LumiBlock::STREAM],
			  coral::AttributeSpecification::
			  typeNameForId(typeid(std::string)));
    
  std::vector<std::string> conditions;
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::SFOID],
				      "sfo"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::RUNNR],
				      "runnr"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::STATE],
				      "state"));
  
  std::string condition = Table::buildANDCond(conditions);
  
  coral::AttributeList updateData;
  updateData.extend<std::string>("sfo");
  updateData[0].data<std::string>() = sfo;
  updateData.extend<unsigned int>("runnr");
  updateData[1].data<unsigned int>() = runnr;
  std::string state(LumiBlockStateNames[LumiBlock::OPENED]);
  updateData.extend<std::string>("state");
  updateData[2].data<std::string>() = state;
  
  query->setCondition( condition, updateData );
  query->setDistinct();   	
  
  try{
    coral::ICursor& cursor = query->execute();
    
    while(cursor.next()){
      lbs[cursor.currentRow()[LumiBlockFieldNames[LumiBlock::LUMIBLOCKNR]].data<unsigned int>()].push_back(std::pair<std::string, std::string>(
cursor.currentRow()[LumiBlockFieldNames[LumiBlock::STREAMTYPE]].data<std::string>(), cursor.currentRow()[LumiBlockFieldNames[LumiBlock::STREAM]].data<std::string>()));
    
    }
  
  } catch(coral::Exception & ex){
    tr.rollback();
    std::ostringstream oss;
    oss << "Search of opened lumiblocks and streams in table "
	<< m_table_name << " failed.";
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str(), ex);
    throw issue;
  }
  return lbs;
}

bool 
SFOTZ::LumiBlockTable::allLumiBlockClosed(const std::string& sfo,
					  const unsigned int& runnr,
					  bool transactive){
  
  SFOTZ::Transaction tr(m_db,transactive);
  
  std::unique_ptr<coral::IQuery> query(Table::newFreeQuery());
  query->addToTableList(m_table_name);
 
  query->addToOutputList(LumiBlockFieldNames[LumiBlock::LUMIBLOCKNR]);
  query->defineOutputType(LumiBlockFieldNames[LumiBlock::LUMIBLOCKNR],
			  coral::AttributeSpecification::
			  typeNameForId(typeid(unsigned int)));
  
  std::vector<std::string> conditions;

  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::SFOID],
				      "sfo"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::RUNNR],
				      "runnr"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::STATE],
				      "state"));
  
  std::string condition = Table::buildANDCond(conditions);
 
  coral::AttributeList updateData;
  updateData.extend<std::string>("sfo");
  updateData[0].data<std::string>() = sfo;
  updateData.extend<unsigned int>("runnr");
  updateData[1].data<unsigned int>() = runnr;
  std::string state(LumiBlockStateNames[LumiBlock::OPENED]);
  updateData.extend<std::string>("state");
  updateData[2].data<std::string>() = state;
 
  query->setCondition( condition, updateData);
  query->setDistinct();   	
  try{
    coral::ICursor& cursor = query->execute();
    return !cursor.next();
  } catch(coral::Exception & ex){
    tr.rollback();
    std::ostringstream oss;
    oss << "Check of LUMI entries state in table " << m_table_name 
	<< " failed.";
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str(), ex);
    throw issue;
  }
}


std::vector<unsigned int> 
SFOTZ::LumiBlockTable::alreadyNotOpenedLumiBlocks(const std::string& sfo,
						  const unsigned int& runnr,
						  bool transactive){
  SFOTZ::Transaction tr(m_db,transactive);
  std::vector<unsigned int> lbs;

  std::unique_ptr<coral::IQuery> query(Table::newFreeQuery());
  query->addToTableList(m_table_name);
 
  query->addToOutputList(LumiBlockFieldNames[LumiBlock::LUMIBLOCKNR]);
  query->defineOutputType(LumiBlockFieldNames[LumiBlock::LUMIBLOCKNR],
			  coral::AttributeSpecification::
			  typeNameForId(typeid(unsigned int)));
  
  std::vector<std::string> conditions;

  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::SFOID],
				      "sfo"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::RUNNR],
				      "runnr"));
  conditions.push_back(Table::
		       disequalCondition(LumiBlockFieldNames[LumiBlock::STATE],
					 "state"));
  
  coral::IQueryDefinition& sub = query->applySetOperation( coral::IQueryDefinition::Minus );
  sub.addToTableList(m_table_name);
  sub.addToOutputList(LumiBlockFieldNames[LumiBlock::LUMIBLOCKNR]);
 
  std::vector<std::string> subconditions;

  subconditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::SFOID],
				      "subsfo"));
  subconditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::RUNNR],
				      "subrunnr"));
  subconditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::STATE],
				      "substate"));
  std::string subcondition = Table::buildANDCond(subconditions);

  
  coral::AttributeList subUpdateData;
  subUpdateData.extend<std::string>("subsfo");
  subUpdateData[0].data<std::string>() = sfo;
  subUpdateData.extend<unsigned int>("subrunnr");
  subUpdateData[1].data<unsigned int>() = runnr;
  std::string substate(LumiBlockStateNames[LumiBlock::OPENED]);
  subUpdateData.extend<std::string>("substate");
  subUpdateData[2].data<std::string>() = substate;

  sub.setCondition( subcondition, subUpdateData);
  sub.setDistinct();   	

  std::string condition = Table::buildANDCond(conditions);

  coral::AttributeList updateData;
  updateData.extend<std::string>("sfo");
  updateData[0].data<std::string>() = sfo;
  updateData.extend<unsigned int>("runnr");
  updateData[1].data<unsigned int>() = runnr;
  std::string state(LumiBlockStateNames[LumiBlock::OPENED]);
  updateData.extend<std::string>("state");
  updateData[2].data<std::string>() = state;

  query->setCondition( condition, updateData );
  query->setDistinct();   	
  try{
    coral::ICursor& cursor = query->execute();
    while(cursor.next()){
      lbs.push_back(cursor.currentRow()[LumiBlockFieldNames[LumiBlock::LUMIBLOCKNR]].data<unsigned int>());
    }
  } catch(coral::Exception & ex){
    tr.rollback();
    std::ostringstream oss;
    oss << "Query on table " << m_table_name << " failed.";
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str(), ex);
    throw issue;
  }
  return lbs;
}

bool
SFOTZ::LumiBlockTable::allLumiBlockTransferred(const std::string& sfo,
					       const unsigned int& runnr,
					       const std::string& streamtype,
					       const std::string& stream,
					       bool transactive){

  SFOTZ::Transaction tr(m_db,transactive);
  std::unique_ptr<coral::IQuery> query(Table::newQuery());
  query->addToOutputList(LumiBlockFieldNames[LumiBlock::LUMIBLOCKNR]);
  query->defineOutputType(LumiBlockFieldNames[LumiBlock::LUMIBLOCKNR],
			  coral::AttributeSpecification::
			  typeNameForId(typeid(unsigned int)));
     
  std::vector<std::string> conditions;
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::SFOID],
				      "sfo"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::RUNNR],
				      "runnr"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::
							  STREAMTYPE],
				      "streamtype"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::STREAM],
				      "stream"));
  conditions.push_back(Table::
		       disequalCondition(LumiBlockFieldNames[LumiBlock::STATE],
					 "state"));
  
  std::string condition = Table::buildANDCond(conditions);

  coral::AttributeList updateData;
  updateData.extend<std::string>("sfo");
  updateData[0].data<std::string>() = sfo;
  updateData.extend<unsigned int>("runnr");
  updateData[1].data<unsigned int>() = runnr;
  updateData.extend<std::string>("streamtype");
  updateData[2].data<std::string>() = streamtype;
  updateData.extend<std::string>("stream");
  updateData[3].data<std::string>() = stream;
  std::string state(LumiBlockStateNames[LumiBlock::TRANSFERRED]);
  updateData.extend<std::string>("state");
  updateData[4].data<std::string>() = state;
  
  query->setCondition( condition, updateData);
  query->setDistinct();   	
  try{
    
    coral::ICursor& cursor = query->execute();
    return !(cursor.next());
    
  } catch(coral::Exception & ex){
    tr.rollback();
    std::ostringstream oss;
    oss << "Check of LUMI entries state on table " 
	<< m_table_name << " failed.";
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str(), ex);
    throw issue;
  }
}
    

std::vector<std::pair<std::string, std::string> >
SFOTZ::LumiBlockTable::getStreams(const std::string&  sfo,
				  const unsigned int& runnr,
				  const unsigned int& lbnr,
				  bool transactive){

  std::vector<std::pair<std::string, std::string> > streams;
  SFOTZ::Transaction tr(m_db,transactive);
  std::unique_ptr<coral::IQuery> query(Table::newQuery());
  query->addToOutputList(LumiBlockFieldNames[LumiBlock::STREAMTYPE]);
  query->defineOutputType(LumiBlockFieldNames[LumiBlock::STREAMTYPE],
			  coral::AttributeSpecification::
			  typeNameForId(typeid(std::string)));
  query->addToOutputList(LumiBlockFieldNames[LumiBlock::STREAM]);
  query->defineOutputType(LumiBlockFieldNames[LumiBlock::STREAM],
			  coral::AttributeSpecification::
			  typeNameForId(typeid(std::string)));
     
  std::vector<std::string> conditions;
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::SFOID],
				      "sfo"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::RUNNR],
				      "runnr"));
  conditions.push_back(Table::
		       equalCondition(LumiBlockFieldNames[LumiBlock::
							  LUMIBLOCKNR],
				      "lbnr"));

  std::string condition = Table::buildANDCond(conditions);

  coral::AttributeList updateData;
  updateData.extend<std::string>("sfo");
  updateData[0].data<std::string>() = sfo;
  updateData.extend<unsigned int>("runnr");
  updateData[1].data<unsigned int>() = runnr;
  updateData.extend<unsigned int>("lbnr");
  updateData[2].data<unsigned int>() = lbnr;
  
  query->setCondition( condition, updateData );
  query->setDistinct();
  try{
    
    coral::ICursor& cursor = query->execute();
    while(cursor.next()){
      streams.push_back(std::pair<std::string, std::string>(cursor.currentRow()[LumiBlockFieldNames[LumiBlock::STREAMTYPE]].data<std::string>(), 
							    cursor.currentRow()[LumiBlockFieldNames[LumiBlock::STREAM]].data<std::string>()));
    }
    
  } catch(coral::Exception & ex){
    tr.rollback();
    std::ostringstream oss;
    oss << "Cannot get existing streams from table " 
	<< m_table_name;
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str(), ex);
    throw issue;
  }
  return streams;
}
	
