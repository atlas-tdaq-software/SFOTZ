// Dear emacs, this is -*- c++ -*-

/**
 * @file RunTable.cxx
 * @author <a href="mailto:Wainer.vandelli@cern.ch">Wainer Vandelli</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Declares the SFOTZ::RunTable.
 */

#include "SFOTZ/RunTable.h"
#include "SFOTZ/SFOTZIssues.h"
#include "CoralBase/TimeStamp.h"

namespace SFOTZ {
static char * RunStateNames[3]={(char *)"OPENED", (char *)"CLOSED",
				(char *)"TRANSFERRED"};

static char * RunFieldNames[]={(char *)"SFOID", (char *)"RUNNR",
				 (char *)"STREAMTYPE", (char *)"STREAM",
				 (char *)"STATE", (char *) "TZSTATE",
				 (char *)"DSNAME", (char *) "PROJECT",
				 (char *)"T_STAMP",
				 (char *)"RUN_SEQNUM",
				 (char *)"MAXNRSFOS",
				 (char *)"TRANSFERSTATE",
         };

  namespace Run{
    enum States{OPENED, CLOSED, TRANSFERRED};
    enum Fields{SFOID, RUNNR, STREAMTYPE, STREAM, STATE, TZSTATE,
		DSNAME, PROJECT, T_STAMP, RUN_SEQNUM, MAXNRSFOS, TRANSFERSTATE};
  }

}

SFOTZ::RunTable::RunTable(SFOTZ::DBConnection& db,
			  const std::string& tablename,
			  bool createifmissing)
  :Table(db,tablename,createifmissing,createDescription(tablename)){
}

SFOTZ::RunTable::~RunTable(){
}


coral::TableDescription *
SFOTZ::RunTable::createDescription(const std::string& tablename){

  coral::TableDescription * description = new coral::TableDescription("");
  description->setName(tablename);
  description->insertColumn(RunFieldNames[Run::SFOID],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 32, false);
  description->insertColumn(RunFieldNames[Run::RUNNR],
			   coral::AttributeSpecification::typeNameForId( typeid(unsigned int) ) );
  description->insertColumn(RunFieldNames[Run::STREAMTYPE],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 32, false);
  description->insertColumn(RunFieldNames[Run::STREAM],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 32, false);
  description->insertColumn(RunFieldNames[Run::STATE],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 32, false);
  description->insertColumn(RunFieldNames[Run::TZSTATE],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 32, false);
  description->insertColumn(RunFieldNames[Run::DSNAME],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 256, false);
  description->insertColumn(RunFieldNames[Run::PROJECT],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 64, false);
  description->insertColumn(RunFieldNames[Run::T_STAMP],
			   coral::AttributeSpecification::typeNameForId( typeid(coral::TimeStamp) ) );
  description->insertColumn(RunFieldNames[Run::RUN_SEQNUM],
			   coral::AttributeSpecification::typeNameForId( typeid(unsigned int) ) );
  description->insertColumn(RunFieldNames[Run::MAXNRSFOS],
			   coral::AttributeSpecification::typeNameForId( typeid(unsigned int) ) );
  description->insertColumn(RunFieldNames[Run::TRANSFERSTATE],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 32, false);

  std::vector<std::string> keycomponents;
  keycomponents.push_back(RunFieldNames[Run::SFOID]);
  keycomponents.push_back(RunFieldNames[Run::RUNNR]);
  keycomponents.push_back(RunFieldNames[Run::STREAMTYPE]);
  keycomponents.push_back(RunFieldNames[Run::STREAM]);

  description->setPrimaryKey(keycomponents);

  description->createIndex(tablename+"_SEQINDEX",
			   RunFieldNames[Run::RUN_SEQNUM],
			   true);

  return description;
}

bool
SFOTZ::RunTable::alreadyExist(const std::string& sfo,
			      const unsigned int& runnr,
			      const std::string& streamtype,
			      const std::string& stream,
			      bool transactive){

  SFOTZ::Transaction tr(m_db,transactive);
  std::unique_ptr<coral::IQuery> query(Table::newQuery());
  query->addToOutputList(RunFieldNames[Run::SFOID]);
  query->defineOutputType(RunFieldNames[Run::SFOID],
			  coral::AttributeSpecification::typeNameForId(typeid (std::string)));

  std::vector<std::string> conditions;

  conditions.push_back(Table::
		       equalCondition(RunFieldNames[Run::SFOID],"sfo"));
  conditions.push_back(Table::
		       equalCondition(RunFieldNames[Run::RUNNR],"runnr"));
  conditions.push_back(Table::
		       equalCondition(RunFieldNames[Run::STREAMTYPE],
				      "streamtype"));
  conditions.push_back(Table::
		       equalCondition(RunFieldNames[Run::STREAM],"stream"));

  std::string condition = Table::buildANDCond(conditions);

  coral::AttributeList conditionData;
  conditionData.extend<std::string>("sfo");
  conditionData[0].data<std::string>() = sfo;
  conditionData.extend<unsigned int>("runnr");
  conditionData[1].data<unsigned int>() = runnr;
  conditionData.extend<std::string>("streamtype");
  conditionData[2].data<std::string>() = streamtype;
  conditionData.extend<std::string>("stream");
  conditionData[3].data<std::string>() = stream;

  query->setCondition( condition, conditionData );
  try{
    coral::ICursor& cursor = query->execute();
    return cursor.next();
  } catch(coral::Exception & ex){
    tr.rollback();
    std::ostringstream oss;
    oss << "Query failed. Table: " << m_table_name;
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str(), ex);
    throw issue;
  }
}


void
SFOTZ::RunTable::addNewRunEntry(const std::string& sfo,
				const unsigned int& runnr,
				const std::string& streamtype,
				const std::string& stream,
				const std::string& state,
				const std::string& dsname,
				const std::string& project,
				const unsigned int& maxnrsfos,
				SFOTZ::IndexTable * index,
				bool transactive){

  if(!alreadyExist(sfo,runnr,streamtype,stream,transactive)){
    coral::AttributeList buffer;
    for(int i=0;i<m_descr->numberOfColumns();i++){
      const coral::IColumn &column = m_descr->columnDescription(i);
      buffer.extend (column.name(), column.type());
    }

    std::string colname(RunFieldNames[Run::RUN_SEQNUM]);
    unsigned int next_index = index->
      getNextIndex(colname,transactive);

    buffer[Run::SFOID].data<std::string>() = sfo;
    buffer[Run::RUNNR].data<unsigned int>() = runnr;
    buffer[Run::STREAMTYPE].data<std::string>() = streamtype;
    buffer[Run::STREAM].data<std::string>() = stream;
    buffer[Run::STATE].data<std::string>() = state;
    buffer[Run::TZSTATE].data<std::string>() = "NONE";
    buffer[Run::DSNAME].data<std::string>() = dsname;
    buffer[Run::PROJECT].data<std::string>() = project;
    buffer[Run::T_STAMP].data<coral::TimeStamp>() = coral::TimeStamp::now();
    buffer[Run::RUN_SEQNUM].data<unsigned int>() = next_index;
    buffer[Run::MAXNRSFOS].data<unsigned int>() = maxnrsfos;
    buffer[Run::TRANSFERSTATE].data<std::string>() = "NONE";

    Table::insertBuffer(buffer,transactive);
  }else{
    std::ostringstream oss;
    oss << "Insert skipped, entry already exists. Table: " << m_table_name
	<< " SFOID=" << sfo
	<< " RUNNR=" << runnr
	<< " STREAMTYPE=" << streamtype
	<< " STREAM="<< stream;
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str());
    ers::warning(issue);
  }
}

void
SFOTZ::RunTable::addOpenedRunEntry(const std::string& sfo,
				   const unsigned int& runnr,
				   const std::string& streamtype,
				   const std::string& stream,
				   const std::string& dsname,
				   const std::string& project,
				   const unsigned int& maxnrsfos,
				   SFOTZ::IndexTable * index,
				   bool transactive){

  std::string state(RunStateNames[Run::OPENED]);
  addNewRunEntry(sfo, runnr,
		 streamtype, stream,
		 state,
		 dsname,
		 project,
		 maxnrsfos,
		 index,
		 transactive);
}


long
SFOTZ::RunTable::changeEntryState(const std::string& sfo,
				  const unsigned int& runnr,
				  const std::string& streamtype,
				  const std::string& stream,
				  const std::string state,
				  const std::string oldstate,
				  bool transactive){


  std::vector<std::string> actions;
  actions.push_back(Table::updateAction(RunFieldNames[Run::STATE],
					"newstate"));
  actions.push_back(Table::updateAction(RunFieldNames[Run::T_STAMP],
					"newtime"));

  if (state == RunStateNames[Run::TRANSFERRED]) {
    /* 2023-10: Tier 0 requested that a stream is marked with
     * TRANSFERSTATE='TRANSFERRING' when at least a file has been transferred
     * for this stream. This is done on the CastorScript side. When all files
     * have been transferred, i.e., when the stream state is set to
     * 'TRANSFERRED', TRANSFERSTATE is set to 'TRANSFERRED' together with STATE
     * for consistency.
     * This feature makes it possible for Tier 0 to optimize their polling of
     * the database in case no files are ever transferred for entries existing
     * in the database (it happens for calibration runs, when detectors decide
     * a posteriori not to transfer the data to EOS).
     */
    actions.push_back(Table::updateAction(RunFieldNames[Run::TRANSFERSTATE],
				"newtransferstate"));
  }

  std::string updateAction = Table::buildList(actions);

  std::vector<std::string> conditions;
  conditions.push_back(Table::equalCondition(RunFieldNames[Run::SFOID],
					     "sfo"));
  conditions.push_back(Table::equalCondition(RunFieldNames[Run::RUNNR],
					     "runnr"));
  conditions.push_back(Table::equalCondition(RunFieldNames[Run::STREAMTYPE],
					     "streamtype"));
  conditions.push_back(Table::equalCondition(RunFieldNames[Run::STREAM],
					     "stream"));
  if(oldstate!=""){
    conditions.push_back(Table::equalCondition(RunFieldNames[Run::STATE],
					       "oldstate"));
  }

  std::string updateCondition = Table::buildANDCond(conditions);

  coral::AttributeList updateData;
  updateData.extend<std::string>("newstate");
  updateData[0].data<std::string>() = state;
  updateData.extend<coral::TimeStamp>("newtime");
  updateData[1].data<coral::TimeStamp>() = coral::TimeStamp::now();
  updateData.extend<std::string>("sfo");
  updateData[2].data<std::string>() = sfo;
  updateData.extend<unsigned int>("runnr");
  updateData[3].data<unsigned int>() = runnr;
  updateData.extend<std::string>("streamtype");
  updateData[4].data<std::string>() = streamtype;
  updateData.extend<std::string>("stream");
  updateData[5].data<std::string>() = stream;
  if (state == RunStateNames[Run::TRANSFERRED]) {
    updateData.extend<std::string>("newtransferstate");
    updateData[updateData.size() - 1].data<std::string>() = RunStateNames[Run::TRANSFERRED];
  }
  if(oldstate!=""){
      updateData.extend<std::string>("oldstate");
      updateData[updateData.size() - 1].data<std::string>() = oldstate;
  }

  return Table::updateEntry(updateAction,updateCondition,
			    updateData,transactive);
}

void
SFOTZ::RunTable::changeAllEntryState(const std::string& sfo,
				     const unsigned int& runnr,
				     const std::string state,
				     const std::string oldstate,
				     bool transactive){


  std::vector<std::string> actions;
  actions.push_back(Table::updateAction(RunFieldNames[Run::STATE],
					"newstate"));
  actions.push_back(Table::updateAction(RunFieldNames[Run::T_STAMP],
					"newtime"));

  std::string updateAction = Table::buildList(actions);

  std::vector<std::string> conditions;
  conditions.push_back(Table::equalCondition(RunFieldNames[Run::SFOID],
					     "sfo"));
  conditions.push_back(Table::equalCondition(RunFieldNames[Run::RUNNR],
					     "runnr"));
  if(oldstate!=""){
    conditions.push_back(Table::
			 equalCondition(RunFieldNames[Run::STATE],
					"oldstate"));
  }

  std::string updateCondition = Table::buildANDCond(conditions);

  coral::AttributeList updateData;
  updateData.extend<std::string>("newstate");
  updateData[0].data<std::string>() = state;
  updateData.extend<coral::TimeStamp>("newtime");
  updateData[1].data<coral::TimeStamp>() = coral::TimeStamp::now();
  updateData.extend<std::string>("sfo");
  updateData[2].data<std::string>() = sfo;
  updateData.extend<unsigned int>("runnr");
  updateData[3].data<unsigned int>() = runnr;
  if(oldstate!=""){
      updateData.extend<std::string>("oldstate");
      updateData[4].data<std::string>() = oldstate;
  }


  Table::updateEntry(updateAction,updateCondition,updateData,transactive);
}

void
SFOTZ::RunTable::setRunEntryClosed(const std::string& sfo,
				   const unsigned int& runnr,
				   const std::string& streamtype,
				   const std::string& stream,
				   bool transactive){

  long nrows = changeEntryState(sfo, runnr,
				streamtype, stream,
				RunStateNames[Run::CLOSED],
				RunStateNames[Run::OPENED],
				transactive);
  if(nrows!=1){
    std::ostringstream oss;
    oss << "Update action on table: " << m_table_name
	<<" affected "
	<< nrows << " rows"
	<< " instead of 1." << std::endl
	<< " Action: RUN CLOSED"
	<< " RUNNR=" << runnr
	<< " STREAMTYPE=" << streamtype
	<< " STREAM="<< stream;
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str());
    throw issue;
  }
}


void
SFOTZ::RunTable::setRunEntryTransferred(const std::string& sfo,
					const unsigned int& runnr,
					const std::string& streamtype,
					const std::string& stream,
					bool transactive){

  long nrows = changeEntryState(sfo, runnr,
				streamtype, stream,
				RunStateNames[Run::TRANSFERRED],
				RunStateNames[Run::CLOSED],
				transactive);
  if(nrows!=1){
    std::ostringstream oss;
    oss << "Update action on table: " << m_table_name
	<<" affected "
	<< nrows << " rows"
	<< " instead of 1."<< std::endl
	<< "Action: RUN TRANSFERRED"
	<< " RUNNR=" << runnr
	<< " STREAMTYPE=" << streamtype
	<< " STREAM="<< stream;
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str());
    throw issue;
  }
}


void
SFOTZ::RunTable::setAllRunEntryClosed(const std::string& sfo,
				      const unsigned int& runnr,
				      bool transactive){

  changeAllEntryState(sfo, runnr,
		      RunStateNames[Run::CLOSED],
		      RunStateNames[Run::OPENED],
		      transactive);
}


void
SFOTZ::RunTable::setAllRunEntryTransferred(const std::string& sfo,
					   const unsigned int& runnr,
					   bool transactive){

  changeAllEntryState(sfo, runnr,
		      RunStateNames[Run::TRANSFERRED],
		      RunStateNames[Run::CLOSED],
		      transactive);
}


bool
SFOTZ::RunTable::entriesAlreadyExist(const std::string& sfo,
				     const unsigned int& runnr,
				     bool transactive){

  SFOTZ::Transaction tr(m_db,transactive);
  std::unique_ptr<coral::IQuery> query(Table::newQuery());
  query->addToOutputList(RunFieldNames[Run::SFOID]);
  query->defineOutputType(RunFieldNames[Run::SFOID],
			  coral::AttributeSpecification::typeNameForId(typeid (std::string)));

  std::vector<std::string> conditions;

  conditions.push_back(Table::equalCondition(RunFieldNames[Run::SFOID],
					     "sfo"));
  conditions.push_back(Table::equalCondition(RunFieldNames[Run::RUNNR],
					     "runnr"));

  std::string condition = Table::buildANDCond(conditions);

  coral::AttributeList updateData;
  updateData.extend<std::string>("sfo");
  updateData[0].data<std::string>() = sfo;
  updateData.extend<unsigned int>("runnr");
  updateData[1].data<unsigned int>() = runnr;

  query->setCondition( condition, updateData );
  try{
    coral::ICursor& cursor = query->execute();
    return cursor.next();
  } catch(coral::Exception & ex){
    tr.rollback();
    std::ostringstream oss;
    oss << "Check for existing RUN entries in the DB failed. Table: "
	<< m_table_name
	<< " RUNNR=" << runnr
	<< " SFOID=" << sfo;
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str(), ex);
    throw issue;
  }
}
