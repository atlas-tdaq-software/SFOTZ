// Dear emacs, this is -*- c++ -*-

/**
 * @file Transaction.cxx
 * @author <a href="mailto:Wainer.vandelli@cern.ch">Wainer Vandelli</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Declares the SFOTZ::Transaction.
 */

#include "SFOTZ/Transaction.h"
#include "SFOTZ/SFOTZIssues.h"

#include <ostream>

SFOTZ::Transaction::Transaction(SFOTZ::DBConnection& db,  bool transactive,
				bool readonly)
  :m_db(db),
   m_reuse(transactive)
{
  if(!m_reuse)
    try{
      m_db.getTransaction().start(readonly);
    }catch(const coral::Exception & exc) {
      rollback();
      SFOTZ::TransactionIssue issue(ERS_HERE, "Cannot start transaction", exc);
      throw issue;
    }
  m_open=true;
}

SFOTZ::Transaction::~Transaction(){
  if(m_open && !m_reuse){ 
    commit();
  }
}

void
SFOTZ::Transaction::commit(){
  m_open = false;
  if(!m_reuse){
    try{
      m_db.getTransaction().commit();
      m_db.send_commands_for_cb();
    }catch(const coral::Exception & exc) {
      rollback();
      SFOTZ::TransactionIssue issue(ERS_HERE, "Commit failed. Changes deleted", exc);
      throw issue;
    } 
  }
}

void
SFOTZ::Transaction::rollback(){
  if(!m_reuse) {
    m_db.getTransaction().rollback();
    m_db.clear_commands_for_cb();
  }
  m_open = false;
}
