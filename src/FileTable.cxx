// Dear emacs, this is -*- c++ -*-

/**
 * @file FileTable.cxx
 * @author <a href="mailto:Wainer.vandelli@cern.ch">Wainer Vandelli</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Declares the SFOTZ::FileTable.
 */

#include "SFOTZ/FileTable.h"
#include "SFOTZ/SFOTZIssues.h"
#include "CoralBase/TimeStamp.h"
#include <time.h>

namespace SFOTZ {
  static char * HealthStateNames[2]={(char *)"SANE", (char *)"TRUNCATED"};

  static char * TransferStateNames[3]={(char *)"ONDISK", (char *)"TRANSFERRED",
				       (char *)"STAGED"};

  static char * FileStateNames[3]={(char *)"OPENED", (char *)"CLOSED", 
				   (char *)"DELETED"};
  
  static char * FileFieldNames[23]={(char *) "LFN", (char *) "FILENR", 
				    (char *) "SFOID", (char *) "SFOHOST",
				    (char *) "RUNNR", (char *) "LUMIBLOCKNR",
				    (char *) "STREAMTYPE", (char *) "STREAM",
				    (char *) "TRANSFERSTATE", 
				    (char *) "FILESTATE",
				    (char *) "FILEHEALTH", (char *) "TZSTATE",
				    (char *) "FILESIZE",
				    (char *) "GUID", (char *) "CHECKSUM",
				    (char *) "PFN", (char *) "SFOPFN",
				    (char *) "NREVENTS", (char *) "T_STAMP",
                                    (char *) "ENDTIME", 
				    (char *) "ENDTIME_EPOCH",
				    (char *) "FILE_SEQNUM",
				    (char *) "MAXNRSFOS"};

  namespace File{
    enum FileFields{LFN, FILENR, SFOID, SFOHOST,
		    RUNNR, LUMIBLOCKNR, 
		    STREAMTYPE, STREAM, 
		    TRANSFERSTATE, FILESTATE, FILEHEALTH, TZSTATE,
		    FILESIZE, GUID, CHECKSUM, 
		    PFN, SFOPFN, NREVENTS,
		    T_STAMP, ENDTIME, ENDTIME_EPOCH,
		    FILE_SEQNUM, MAXNRSFOS};
    enum HealthStates{SANE, TRUNCATED};
    enum TransferStates{ONDISK, TRANSFERRED, STAGED};
    enum FileStates{OPENED, CLOSED, DELETED};
  }
}

SFOTZ::FileTable::FileTable(SFOTZ::DBConnection& db, 
			    const std::string& tablename,
			    bool createifmissing)
  :Table(db,tablename,createifmissing,createDescription(tablename)){
}

SFOTZ::FileTable::~FileTable(){
}


coral::TableDescription *
SFOTZ::FileTable::createDescription(const std::string& tablename){

  coral::TableDescription * description = new coral::TableDescription("");
  description->setName(tablename);
  description->insertColumn(FileFieldNames[File::LFN],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 256, false );
  description->insertColumn(FileFieldNames[File::FILENR],
			   coral::AttributeSpecification::typeNameForId( typeid(unsigned int) ) );
  description->insertColumn(FileFieldNames[File::SFOID],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 32, false );
  description->insertColumn(FileFieldNames[File::SFOHOST],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 64, false );
  description->insertColumn(FileFieldNames[File::RUNNR],
			   coral::AttributeSpecification::typeNameForId( typeid(unsigned int) ) );
  description->insertColumn(FileFieldNames[File::LUMIBLOCKNR],
			   coral::AttributeSpecification::typeNameForId( typeid(unsigned int) ) );
  description->insertColumn(FileFieldNames[File::STREAMTYPE],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 32, false );
  description->insertColumn(FileFieldNames[File::STREAM],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 32, false );
    description->insertColumn(FileFieldNames[File::TRANSFERSTATE],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 32, false );
  description->insertColumn(FileFieldNames[File::FILESTATE],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 32, false );
  description->insertColumn(FileFieldNames[File::FILEHEALTH],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 32, false );
  description->insertColumn(FileFieldNames[File::TZSTATE],
			    coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 32, false );
  description->insertColumn(FileFieldNames[File::FILESIZE],
			   coral::AttributeSpecification::typeNameForId( typeid(uint64_t) ) );
  description->insertColumn(FileFieldNames[File::GUID],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 64, false );
  description->insertColumn(FileFieldNames[File::CHECKSUM],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 64, false );
    description->insertColumn(FileFieldNames[File::PFN],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 1024, false );
  description->insertColumn(FileFieldNames[File::SFOPFN],
			   coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 1024, false );
  description->insertColumn(FileFieldNames[File::NREVENTS],
			   coral::AttributeSpecification::typeNameForId( typeid(unsigned int) ) );
  description->insertColumn(FileFieldNames[File::T_STAMP],
			    coral::AttributeSpecification::typeNameForId( typeid(coral::TimeStamp)));
  description->insertColumn(FileFieldNames[File::ENDTIME],
			    coral::AttributeSpecification::typeNameForId( typeid(coral::TimeStamp) ) ); 
  description->insertColumn(FileFieldNames[File::ENDTIME_EPOCH],
			    coral::AttributeSpecification::typeNameForId( typeid(unsigned int) ) ); 
  description->insertColumn(FileFieldNames[File::FILE_SEQNUM],
			    coral::AttributeSpecification::typeNameForId( typeid(unsigned int) ) ); 
  
  std::vector<std::string> keycomponents;
  keycomponents.push_back(FileFieldNames[File::FILENR]);
  keycomponents.push_back(FileFieldNames[File::SFOID]);
  keycomponents.push_back(FileFieldNames[File::RUNNR]);
  keycomponents.push_back(FileFieldNames[File::LUMIBLOCKNR]);
  keycomponents.push_back(FileFieldNames[File::STREAMTYPE]);
  keycomponents.push_back(FileFieldNames[File::STREAM]);

  description->setPrimaryKey(keycomponents);

  description->setUniqueConstraint(FileFieldNames[File::LFN],
				   tablename + "_" + 
				   std::string(FileFieldNames[File::LFN]));
  description->setUniqueConstraint(FileFieldNames[File::SFOPFN],
				   tablename + "_" + 
                                   std::string(FileFieldNames[File::SFOPFN]));
  description->setUniqueConstraint(FileFieldNames[File::PFN],
				   tablename + "_" + 
                                   std::string(FileFieldNames[File::PFN]));

  description->createIndex(tablename+"_SEQINDEX",
			   FileFieldNames[File::FILE_SEQNUM],
			   true);

  return description;
}

bool 
SFOTZ::FileTable::alreadyExist(const unsigned int& filenr,
			       const std::string& sfo,
			       const unsigned int& runnr,
			       const unsigned int& lbnr,
			       const std::string& streamtype,
			       const std::string& stream,
			       bool transactive){

  SFOTZ::Transaction tr(m_db,transactive);
  std::unique_ptr<coral::IQuery> query(Table::newQuery());
  query->addToOutputList(FileFieldNames[File::SFOID]);
  query->defineOutputType(FileFieldNames[File::SFOID],
			  coral::AttributeSpecification::
			  typeNameForId(typeid(std::string)));
  
  std::vector<std::string> conditions;

  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::FILENR],
				      "filenr"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::SFOID],
				      "sfo"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::RUNNR],
				      "runnr"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::
							  LUMIBLOCKNR],
				      "lbnr"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::
							  STREAMTYPE],
				      "streamtype"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::STREAM],
				      "stream"));

  std::string condition = Table::buildANDCond(conditions);

  coral::AttributeList updateData;
  updateData.extend<unsigned int>("filenr");
  updateData[0].data<unsigned int>() = filenr;
  updateData.extend<std::string>("sfo");
  updateData[1].data<std::string>() = sfo;
  updateData.extend<unsigned int>("runnr");
  updateData[2].data<unsigned int>() = runnr;
  updateData.extend<unsigned int>("lbnr");
  updateData[3].data<unsigned int>() = lbnr;
  updateData.extend<std::string>("streamtype");
  updateData[4].data<std::string>() = streamtype;
  updateData.extend<std::string>("stream");
  updateData[5].data<std::string>() = stream;


  query->setCondition( condition, updateData);
  try{
    coral::ICursor& cursor = query->execute();
    return cursor.next();
  } catch(coral::Exception & ex){
    tr.rollback();
    std::ostringstream oss;
    oss << "Query on table " << m_table_name << " failed.";
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str(), ex);
    throw issue;
  }
}


void 
SFOTZ::FileTable::addOpenedFileEntry(const std::string& lfn,
				     const unsigned int& filenr,
				     const std::string& sfo,
				     const std::string& sfohost,
				     const unsigned int& runnr,
				     const unsigned int& lbnr,
				     const std::string& streamtype,
				     const std::string& stream,
				     const std::string& guid,
				     const std::string& sfopfn,
				     SFOTZ::IndexTable * index,
				     bool transactive){
  
  if(!alreadyExist(filenr,sfo,runnr,lbnr,streamtype,stream,transactive)){
    coral::AttributeList buffer;
    
    for(int i=0;i<m_descr->numberOfColumns();i++){
      const coral::IColumn &column = m_descr->columnDescription(i);
      buffer.extend (column.name(), column.type());
    }

    std::string colname(FileFieldNames[File::FILE_SEQNUM]);
    unsigned int next_index = index->
      getNextIndex(colname,transactive);

    buffer[File::LFN].data<std::string>() = lfn;
    buffer[File::FILENR].data<unsigned int>() = filenr;
    buffer[File::SFOID].data<std::string>() = sfo;
    buffer[File::SFOHOST].data<std::string>() = sfohost;
    buffer[File::RUNNR].data<unsigned int>() = runnr;
    buffer[File::LUMIBLOCKNR].data<unsigned int>() = lbnr;
    buffer[File::STREAMTYPE].data<std::string>() = streamtype;
    buffer[File::STREAM].data<std::string>() = stream;
    buffer[File::TRANSFERSTATE].data<std::string>() = 
      std::string(TransferStateNames[File::ONDISK]);
    buffer[File::FILESTATE].data<std::string>() =
      std::string(FileStateNames[File::OPENED]);
    buffer[File::FILEHEALTH].data<std::string>() = 
      std::string(HealthStateNames[File::SANE]);
    buffer[File::TZSTATE].data<std::string>() = "NONE";
    buffer[File::GUID].data<std::string>() = guid;
    buffer[File::PFN].data<std::string>() = lfn;
    buffer[File::SFOPFN].data<std::string>() = sfopfn;
    buffer[File::T_STAMP].data<coral::TimeStamp>() = 
      coral::TimeStamp::now();
    buffer[File::ENDTIME].data<coral::TimeStamp>() = 
      coral::TimeStamp::now();
    buffer[File::FILE_SEQNUM].data<unsigned int>() = next_index;

    Table::insertBuffer(buffer,transactive);
  }else{
    std::ostringstream oss;
    oss << "Insert skipped, entry already exists."
	<< " Table " << m_table_name
	<< " LFN=" << lfn
	<< " FILENR=" << filenr
	<< " SFOID=" << sfo
	<< " RUNNR=" << runnr
	<< " LUMIBLOCKNR=" << lbnr
	<< " STREAMTYPE=" << streamtype
	<< " STREAM="<< stream;
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str());
    ers::warning(issue);
  }

}

 void 
SFOTZ::FileTable::setFileEntryClosed(const std::string& lfn,
				     const unsigned int& filenr,
				     const std::string& sfo,
				     const unsigned int& runnr,
				     const unsigned int& lbnr,
				     const std::string& streamtype,
				     const std::string& stream,
				     const uint64_t& filesize,
				     const std::string& checksum,
				     const unsigned int& nrevents,
				     const std::string& sfopfn,
				     bool transactive){

   
   std::vector<std::string> actions;
   actions.push_back(Table::updateAction(FileFieldNames[File::LFN],
					 "newlfn"));
   actions.push_back(Table::updateAction(FileFieldNames[File::FILESIZE],
					 "newfilesize"));
   actions.push_back(Table::updateAction(FileFieldNames[File::CHECKSUM],
					 "newchecksum"));
   actions.push_back(Table::updateAction(FileFieldNames[File::NREVENTS],
					 "newnrevents"));
   actions.push_back(Table::updateAction(FileFieldNames[File::SFOPFN],
					 "newsfopfn"));
   actions.push_back(Table::updateAction(FileFieldNames[File::T_STAMP], 
					 "newtime"));
   actions.push_back(Table::updateAction(FileFieldNames[File::ENDTIME], 
					 "newendtime"));
   actions.push_back(Table::updateAction(FileFieldNames[File::ENDTIME_EPOCH], 
					 "newendtimeepoch"));
   actions.push_back(Table::updateAction(FileFieldNames[File::FILESTATE], 
					 "newfilestate"));

   std::string updateAction = Table::buildList(actions);
   
   std::vector<std::string> conditions;
   conditions.push_back(Table::
			equalCondition(FileFieldNames[File::FILENR],
				       "filenr"));
   conditions.push_back(Table::
			equalCondition(FileFieldNames[File::SFOID],
				       "sfo"));
   conditions.push_back(Table::
			equalCondition(FileFieldNames[File::RUNNR],
				       "runnr"));
   conditions.push_back(Table::
			equalCondition(FileFieldNames[File::
						      LUMIBLOCKNR],
				       "lbnr"));
   conditions.push_back(Table::
			equalCondition(FileFieldNames[File::
						      STREAMTYPE],
				       "streamtype"));
   conditions.push_back(Table::
			equalCondition(FileFieldNames[File::STREAM],
				       "stream"));
   conditions.push_back(Table::
			equalCondition(FileFieldNames[File::FILESTATE],
				       "oldstate"));
   
   
   std::string updateCondition = Table::buildANDCond(conditions);
   
   coral::AttributeList updateData;
   updateData.extend<std::string>("newfilestate");
   updateData[0].data<std::string>() = FileStateNames[File::CLOSED];
   updateData.extend<std::string>("newlfn");
   updateData[1].data<std::string>() = lfn;
   updateData.extend<uint64_t>("newfilesize");
   updateData[2].data<uint64_t>() = filesize;
   updateData.extend<std::string>("newchecksum");
   updateData[3].data<std::string>() = checksum;
   updateData.extend<unsigned int>("newnrevents");
   updateData[4].data<unsigned int>() = nrevents;
   updateData.extend<std::string>("newsfopfn");
   updateData[5].data<std::string>() = sfopfn;
   updateData.extend<coral::TimeStamp>("newtime");
   updateData[6].data<coral::TimeStamp>() = coral::TimeStamp::now();
   updateData.extend<coral::TimeStamp>("newendtime");
   
   //Use UTC time in the endtime column
   time_t now = ::time(NULL);
   struct tm * utctime = ::gmtime(&now);
   coral::TimeStamp cTime(utctime->tm_year+1900,
			  utctime->tm_mon+1,
			  utctime->tm_mday,
			  utctime->tm_hour,
			  utctime->tm_min,
			  utctime->tm_sec, 
			  0);
   
   updateData[7].data<coral::TimeStamp>() = cTime;
   updateData.extend<unsigned int>("newendtimeepoch");
   updateData[8].data<unsigned int>() = now;
   updateData.extend<unsigned int>("filenr");
   updateData[9].data<unsigned int>() = filenr;
   updateData.extend<std::string>("sfo");
   updateData[10].data<std::string>() = sfo;
   updateData.extend<unsigned int>("runnr");
   updateData[11].data<unsigned int>() = runnr;
   updateData.extend<unsigned int>("lbnr");
   updateData[12].data<unsigned int>() = lbnr;
   updateData.extend<std::string>("streamtype");
   updateData[13].data<std::string>() = streamtype;
   updateData.extend<std::string>("stream");
   updateData[14].data<std::string>() = stream;
   std::string oldstate(FileStateNames[File::OPENED]);
   updateData.extend<std::string>("oldstate");
   updateData[15].data<std::string>() = oldstate;
   

   long nrows = Table::updateEntry(updateAction,updateCondition,
				   updateData,transactive);
   if(nrows!=1){
     std::ostringstream oss;
     oss << "Update action on table " << m_table_name
	 << " affected " 
	 << nrows << " rows "
	 << " instead of 1."<< std::endl
	 << "Action: FILE CLOSED"
	 << " RUNNR=" << runnr
	 << " LUMINR=" << lbnr
	 << " FILENR=" << filenr
	 << " STREAMTYPE=" << streamtype
	 << " STREAM="<< stream
	 << " LFN=" << lfn
	 << " SFOPFN=" << sfopfn;
     std::string tmp = oss.str();
     SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str());
     throw issue;
   }
 }


void 
SFOTZ::FileTable::setFileEntryTransferred(const std::string& lfn,
					  const std::string& pfn,
					  bool transactive){

   std::vector<std::string> actions;
   actions.push_back(Table::updateAction(FileFieldNames[File::TRANSFERSTATE],
					 "newtransstate"));
   actions.push_back(Table::updateAction(FileFieldNames[File::PFN],
					 "newpfn"));
   actions.push_back(Table::updateAction(FileFieldNames[File::T_STAMP], 
					 "newtime"));

   std::string updateAction = Table::buildList(actions);
   
   std::vector<std::string> conditions;
   conditions.push_back(Table::
			equalCondition(FileFieldNames[File::LFN],
				       "lfn"));
   conditions.push_back(Table::
			equalCondition(FileFieldNames[File::TRANSFERSTATE],
				       "transstate"));

   std::string updateCondition = Table::buildANDCond(conditions);
   
   coral::AttributeList updateData;
   updateData.extend<std::string>("newtransstate");
   updateData[0].data<std::string>() = TransferStateNames[File::TRANSFERRED];
   updateData.extend<std::string>("newpfn");
   updateData[1].data<std::string>() = pfn;
   updateData.extend<coral::TimeStamp>("newtime");
   updateData[2].data<coral::TimeStamp>() = coral::TimeStamp::now();
   updateData.extend<std::string>("lfn");
   updateData[3].data<std::string>() = lfn;
   std::string transstate(TransferStateNames[File::ONDISK]);
   updateData.extend<std::string>("transstate");
   updateData[4].data<std::string>() = transstate;


   long nrows = Table::updateEntry(updateAction,updateCondition,
				   updateData,transactive);
   if(nrows!=1){
     std::ostringstream oss;
     oss << "Update action on table " << m_table_name
	 << " affected " 
	 << nrows << " rows "
	 << " instead of 1."<< std::endl
	 << "Action: FILE TRANSFERRED"
	 << " LFN=" << lfn
	 << " PFN=" << pfn;
     std::string tmp = oss.str();
     SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str());
     throw issue;
   }
 }

void 
SFOTZ::FileTable::setFileEntryDeleted(const std::string& lfn,
				      bool transactive){

  std::vector<std::string> actions;
  actions.push_back(Table::updateAction(FileFieldNames[File::FILESTATE],
					"newfilestate"));
  actions.push_back(Table::updateAction(FileFieldNames[File::T_STAMP], 
					"newtime"));
  
  std::string updateAction = Table::buildList(actions);
  
  std::vector<std::string> conditions;
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::LFN],
				      "lfn"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::FILESTATE],
				      "filestate"));

  std::string updateCondition = Table::buildANDCond(conditions);
   
  coral::AttributeList updateData;
  updateData.extend<std::string>("newfilestate");
  updateData[0].data<std::string>() = FileStateNames[File::DELETED];
  updateData.extend<coral::TimeStamp>("newtime");
  updateData[1].data<coral::TimeStamp>() = coral::TimeStamp::now();
  updateData.extend<std::string>("lfn");
  updateData[2].data<std::string>() = lfn;
  std::string filestate(FileStateNames[File::CLOSED]);
  updateData.extend<std::string>("filestate");
  updateData[3].data<std::string>() = filestate;

  long nrows = Table::updateEntry(updateAction,updateCondition,
				  updateData,transactive);
  if(nrows!=1){
    std::ostringstream oss;
    oss << "Update action on table " << m_table_name
	<< " affected " 
	<< nrows << " rows "
	<< " instead of 1."<< std::endl
	<< "Action: FILE DELETED"
	<< " LFN=" << lfn;
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str());
    throw issue;
  }
}

void 
SFOTZ::FileTable::setFileEntryTruncated(const std::string& lfn,
					const unsigned int& filenr,
					const std::string& sfo,
					const unsigned int& runnr,
					const unsigned int& lbnr,
					const std::string& streamtype,
					const std::string& stream,
					const uint64_t& filesize,
					const std::string& sfopfn,
					bool transactive){
  
  std::vector<std::string> actions;
  actions.push_back(Table::updateAction(FileFieldNames[File::LFN],
					"newlfn"));
  actions.push_back(Table::updateAction(FileFieldNames[File::FILESIZE],
					"newfilesize"));
  actions.push_back(Table::updateAction(FileFieldNames[File::SFOPFN],
					"newsfopfn"));
  actions.push_back(Table::updateAction(FileFieldNames[File::T_STAMP], 
					"newtime"));
  actions.push_back(Table::updateAction(FileFieldNames[File::ENDTIME], 
					 "newendtime"));
  actions.push_back(Table::updateAction(FileFieldNames[File::FILESTATE], 
					"newfilestate"));
  actions.push_back(Table::updateAction(FileFieldNames[File::FILEHEALTH], 
					"newfilehealth"));
  
  std::string updateAction = Table::buildList(actions);
  
  std::vector<std::string> conditions;
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::FILENR],
				      "filenr"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::SFOID],
				      "sfo"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::RUNNR],
				      "runnr"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::
						     LUMIBLOCKNR],
				      "lbnr"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::
						     STREAMTYPE],
				      "streamtype"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::STREAM],
				      "stream"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::FILESTATE],
				      "oldstate"));
  
  
  std::string updateCondition = Table::buildANDCond(conditions);
  
  coral::AttributeList updateData;
  updateData.extend<std::string>("newfilestate");
  updateData[0].data<std::string>() = FileStateNames[File::CLOSED];
  updateData.extend<std::string>("newlfn");
  updateData[1].data<std::string>() = lfn;
  updateData.extend<uint64_t>("newfilesize");
  updateData[2].data<uint64_t>() = filesize;
  updateData.extend<std::string>("newsfopfn");
  updateData[3].data<std::string>() = sfopfn;
  updateData.extend<coral::TimeStamp>("newtime");
  updateData[4].data<coral::TimeStamp>() = coral::TimeStamp::now();
  updateData.extend<coral::TimeStamp>("newendtime");
   
  //Use UTC time in the endtime column
  time_t now = ::time(NULL);
  struct tm * utctime = ::gmtime(&now);
  coral::TimeStamp cTime(utctime->tm_year+1900,
			 utctime->tm_mon+1,
			 utctime->tm_mday,
			 utctime->tm_hour,
			 utctime->tm_min,
			 utctime->tm_sec, 
			 0);
  
  updateData[5].data<coral::TimeStamp>() = cTime;
  updateData.extend<std::string>("newfilehealth");
  updateData[6].data<std::string>() = HealthStateNames[File::TRUNCATED];
  updateData.extend<unsigned int>("filenr");
  updateData[7].data<unsigned int>() = filenr;
  updateData.extend<std::string>("sfo");
  updateData[8].data<std::string>() = sfo;
  updateData.extend<unsigned int>("runnr");
  updateData[9].data<unsigned int>() = runnr;
  updateData.extend<unsigned int>("lbnr");
  updateData[10].data<unsigned int>() = lbnr;
  updateData.extend<std::string>("streamtype");
  updateData[11].data<std::string>() = streamtype;
  updateData.extend<std::string>("stream");
  updateData[12].data<std::string>() = stream;
  std::string oldstate(FileStateNames[File::OPENED]);
  updateData.extend<std::string>("oldstate");
  updateData[13].data<std::string>() = oldstate;
   

  long nrows = Table::updateEntry(updateAction,updateCondition,
				  updateData,transactive);
  if(nrows!=1){
    std::ostringstream oss;
    oss << "Update action on table " << m_table_name
	<< " affected " 
	<< nrows << " rows "
	<< " instead of 1."<< std::endl
	<< "Action: FILE TRANSFERRED"
	<< " RUNNR=" << runnr
	<< " LUMINR=" << lbnr
	<< " FILENR=" << filenr
	<< " STREAMTYPE=" << streamtype
	<< " STREAM="<< stream
	<< " LFN=" << lfn
	<< " SFOPFN=" << sfopfn;
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str());
    throw issue;
  }  
}

bool 
SFOTZ::FileTable::allFileTransferred(const std::string& sfo,
				     const unsigned int& runnr,
				     const unsigned int& lbnr,
				     const std::string& streamtype,
				     const std::string& stream,
				     bool transactive){
  SFOTZ::Transaction tr(m_db,transactive);
  std::unique_ptr<coral::IQuery> query(Table::newQuery());
  query->addToOutputList(FileFieldNames[File::LFN]);
  query->defineOutputType(FileFieldNames[File::LFN],
			  coral::AttributeSpecification::
			  typeNameForId(typeid(std::string)));
     
  std::vector<std::string> conditions;
  
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::SFOID],
				      "sfo"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::RUNNR],
				      "runnr"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::LUMIBLOCKNR],
				      "lbnr"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::
							  STREAMTYPE],
				      "streamtype"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::STREAM],
				      "stream"));
  conditions.push_back(Table::
		       disequalCondition(FileFieldNames[File::TRANSFERSTATE],
				      "state"));
  
  std::string condition = Table::buildANDCond(conditions);
  
  coral::AttributeList updateData;
  updateData.extend<std::string>("sfo");
  updateData[0].data<std::string>() = sfo;
  updateData.extend<unsigned int>("runnr");
  updateData[1].data<unsigned int>() = runnr;
  updateData.extend<unsigned int>("lbnr");
  updateData[2].data<unsigned int>() = lbnr;
  updateData.extend<std::string>("streamtype");
  updateData[3].data<std::string>() = streamtype;
  updateData.extend<std::string>("stream");
  updateData[4].data<std::string>() = stream;
  std::string state(TransferStateNames[File::TRANSFERRED]);
  updateData.extend<std::string>("state");
  updateData[5].data<std::string>() = state;
  
  query->setCondition( condition, updateData);
  query->setDistinct();   	
  try{
    
    coral::ICursor& cursor = query->execute();
    return !(cursor.next());
    
  } catch(coral::Exception & ex){
    tr.rollback();
    std::ostringstream oss;
    oss << "Query on table " << m_table_name << " failed.";
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str(), ex);
    throw issue;
  }
}

bool
SFOTZ::FileTable::allLBFileClosed(const std::string& sfo,
				  const unsigned int& runnr,
				  const unsigned int& lbnr,
				  bool transactive){
  
  SFOTZ::Transaction tr(m_db,transactive);
  std::unique_ptr<coral::IQuery> query(Table::newQuery());
  query->addToOutputList(FileFieldNames[File::LFN]);
  query->defineOutputType(FileFieldNames[File::LFN],
			  coral::AttributeSpecification::
			  typeNameForId(typeid(std::string)));
 
  std::vector<std::string> conditions;
 
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::SFOID],
				      "sfo"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::RUNNR],
				      "runnr"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::LUMIBLOCKNR],
				      "lbnr"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::FILESTATE],
				      "state"));
  
  std::string condition = Table::buildANDCond(conditions);

  coral::AttributeList updateData;
  updateData.extend<std::string>("sfo");
  updateData[0].data<std::string>() = sfo;
  updateData.extend<unsigned int>("runnr");
  updateData[1].data<unsigned int>() = runnr;
  updateData.extend<unsigned int>("lbnr");
  updateData[2].data<unsigned int>() = lbnr;
  std::string state(FileStateNames[File::OPENED]);
  updateData.extend<std::string>("state");
  updateData[3].data<std::string>() = state;
  
  query->setCondition( condition, updateData);
  query->setDistinct();   	
  try{
    
    coral::ICursor& cursor = query->execute();
    return !(cursor.next());
    
  } catch(coral::Exception & ex){
    tr.rollback();
    std::ostringstream oss;
    oss << "Query on table " << m_table_name << " failed.";
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str(), ex);
    throw issue;
  }
}

std::vector<std::string> 
SFOTZ::FileTable::getNotClosedAlreadyExistingFiles(const std::string& sfo,
						   const unsigned int& runnr,
						   bool transactive){
  
  SFOTZ::Transaction tr(m_db,transactive);
  std::vector<std::string> sfopfns;
  std::unique_ptr<coral::IQuery> query(Table::newQuery());
  query->addToOutputList(FileFieldNames[File::SFOPFN]);
  query->defineOutputType(FileFieldNames[File::SFOPFN],
			  coral::AttributeSpecification::
			  typeNameForId(typeid(std::string)));
     
  std::vector<std::string> conditions;
  
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::SFOID],
				      "sfo"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::RUNNR],
				      "runnr"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::FILESTATE],
				      "state"));
  
  std::string condition = Table::buildANDCond(conditions);

  coral::AttributeList updateData;
  updateData.extend<std::string>("sfo");
  updateData[0].data<std::string>() = sfo;
  updateData.extend<unsigned int>("runnr");
  updateData[1].data<unsigned int>() = runnr;
  std::string state(FileStateNames[File::OPENED]);
  updateData.extend<std::string>("state");
  updateData[2].data<std::string>() = state;
  
  query->setCondition( condition, updateData);
  query->setDistinct();   	
  try{
    
    coral::ICursor& cursor = query->execute();
    while (cursor.next()){
      sfopfns.push_back(cursor.currentRow()[FileFieldNames[File::SFOPFN]]
			.data<std::string>());
    }
    return sfopfns;
  } catch(coral::Exception & ex){
    tr.rollback();
    std::ostringstream oss;
    oss << "Query on table " << m_table_name << " failed.";
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str(), ex);
    throw issue;
  }
}
   

unsigned int
SFOTZ::FileTable::getMaxIndex(const std::string& sfo,
			      const unsigned int& runnr,
			      const unsigned int& lbnr,
			      const std::string& streamtype,
			      const std::string& stream,
			      bool transactive){

  SFOTZ::Transaction tr(m_db,transactive);
  std::unique_ptr<coral::IQuery> query(Table::newQuery());
  query->addToOutputList(FileFieldNames[File::FILENR]);
  query->defineOutputType(FileFieldNames[File::FILENR],
			  coral::AttributeSpecification::
			  typeNameForId(typeid(unsigned int)));
     
  std::vector<std::string> conditions;
  
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::SFOID],
				      "sfo"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::RUNNR],
				      "runnr"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::LUMIBLOCKNR],
				      "lbnr"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::
							  STREAMTYPE],
				      "streamtype"));
  conditions.push_back(Table::
		       equalCondition(FileFieldNames[File::STREAM],
				      "stream"));
    
  std::string condition = Table::buildANDCond(conditions);
  
  coral::AttributeList updateData;
  updateData.extend<std::string>("sfo");
  updateData[0].data<std::string>() = sfo;
  updateData.extend<unsigned int>("runnr");
  updateData[1].data<unsigned int>() = runnr;
  updateData.extend<unsigned int>("lbnr");
  updateData[2].data<unsigned int>() = lbnr;
  updateData.extend<std::string>("streamtype");
  updateData[3].data<std::string>() = streamtype;
  updateData.extend<std::string>("stream");
  updateData[4].data<std::string>() = stream;
  
  query->setCondition( condition, updateData);
  query->setDistinct();   	
  try{
    
    coral::ICursor& cursor = query->execute();
    unsigned int max=0;
    while (cursor.next()){
      unsigned int curr = cursor.currentRow()[FileFieldNames[File::FILENR]]
	.data<unsigned int>();

      max = (curr > max) ? curr : max;
    }    
    
    return max;

  } catch(coral::Exception & ex){
    tr.rollback();
    std::ostringstream oss;
    oss << "Query on table " << m_table_name << " failed.";
    std::string tmp = oss.str();
    SFOTZ::TableIssue issue(ERS_HERE, tmp.c_str(), ex);
    throw issue;
  }
}
