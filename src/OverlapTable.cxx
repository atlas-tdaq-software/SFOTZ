// Dear emacs, this is -*- c++ -*-

/**
 * @file OverlapTable.cxx
 * @author <a href="mailto:Wainer.vandelli@cern.ch">Wainer Vandelli</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Declares the SFOTZ::OverlapTable.
 */

#include "SFOTZ/OverlapTable.h"
#include "SFOTZ/SFOTZIssues.h"
#include "CoralBase/TimeStamp.h"


namespace SFOTZ {
  static char * OverlapTypeNames[3]={(char *)"EVENTCOUNT",
                                     (char *)"RAW",
				     (char *)"FILE"};
  static char * OverlapFieldNames[9]={(char *)"SFOID", (char *)"RUNNR",
				      (char *)"REFERENCE_STREAM", 
				      (char *)"OVERLAP_STREAM",
				      (char *)"OVERLAP_EVENTS", 
				      (char *)"OVERLAP_RATIO",
				      (char *)"TYPE", (char *)"T_STAMP",
				      (char *)"OVERLAP_SEQNUM"};

}

SFOTZ::OverlapTable::OverlapTable(SFOTZ::DBConnection& db, 
				  const std::string& tablename,
				  bool createifmissing)
  :Table(db,tablename,createifmissing,createDescription(tablename)){
}

SFOTZ::OverlapTable::~OverlapTable(){
}

coral::TableDescription *
SFOTZ::OverlapTable::createDescription(const std::string& tablename){

  coral::TableDescription * description = new coral::TableDescription("");
  description->setName(tablename);
  description->insertColumn(OverlapFieldNames[Overlap::SFOID],
			    coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 32, false);
  description->insertColumn(OverlapFieldNames[Overlap::RUNNR],
			    coral::AttributeSpecification::typeNameForId( typeid(unsigned int) ) );
  description->insertColumn(OverlapFieldNames[Overlap::STREAM1],
			    coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 128, false);
  description->insertColumn(OverlapFieldNames[Overlap::STREAM2],
			    coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 128, false);
  description->insertColumn(OverlapFieldNames[Overlap::OVERLAP],
			    coral::AttributeSpecification::typeNameForId( typeid(unsigned int) ) );
  description->insertColumn(OverlapFieldNames[Overlap::OVERLAP_RATIO],
			    coral::AttributeSpecification::typeNameForId( typeid(float) ) );
  description->insertColumn(OverlapFieldNames[Overlap::TYPE],
			    coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 32, false);
  description->insertColumn(OverlapFieldNames[Overlap::TIMESTAMP],
			    coral::AttributeSpecification::typeNameForId( typeid(coral::TimeStamp) ) );
  description->insertColumn(OverlapFieldNames[Overlap::OVERLAP_SEQNUM],
			    coral::AttributeSpecification::typeNameForId( typeid(unsigned int) ) );
  

  std::vector<std::string> keycomponents;
  keycomponents.push_back(OverlapFieldNames[Overlap::SFOID]);
  keycomponents.push_back(OverlapFieldNames[Overlap::RUNNR]);
  keycomponents.push_back(OverlapFieldNames[Overlap::STREAM1]);
  keycomponents.push_back(OverlapFieldNames[Overlap::STREAM2]);
  keycomponents.push_back(OverlapFieldNames[Overlap::TYPE]);
  description->setPrimaryKey(keycomponents);

  description->createIndex(tablename+"_SEQINDEX",
			   OverlapFieldNames[Overlap::OVERLAP_SEQNUM],
			   true);
  return description;
}

void 
SFOTZ::OverlapTable::addOverlaps(const std::string& sfo,
				 const unsigned int& runnr,
				 const std::vector<SFOTZ::OverlapData>& streams,
				 const SFOTZ::Overlap::Types& overlaptype,
				 SFOTZ::IndexTable * index,
				 bool transactive){
  
  
  coral::AttributeList buffer;
  for(int i=0;i<m_descr->numberOfColumns();i++){
    const coral::IColumn &column = m_descr->columnDescription(i);
    buffer.extend (column.name(), column.type());
  }
  
  std::vector<SFOTZ::OverlapData>::const_iterator  it =
    streams.begin();

  
  std::string colname(OverlapFieldNames[Overlap::OVERLAP_SEQNUM]);
  unsigned int next_index = index->
    getNextIndex(colname, transactive, streams.size());

  for(; it != streams.end(); ++it){
      
    buffer[Overlap::SFOID].data<std::string>() = sfo;
    buffer[Overlap::RUNNR].data<unsigned int>() = runnr;
    buffer[Overlap::STREAM1].data<std::string>() = it->ref_stream;
    buffer[Overlap::STREAM2].data<std::string>() = it->over_stream;
    buffer[Overlap::OVERLAP].data<unsigned int>() = it->overlap;
    buffer[Overlap::OVERLAP_RATIO].data<float>() = it->overlap_ratio;
    buffer[Overlap::TYPE].data<std::string>() = 
      SFOTZ::OverlapTypeNames[overlaptype];
    buffer[Overlap::TIMESTAMP].data<coral::TimeStamp>() = 
      coral::TimeStamp::now();
    buffer[Overlap::OVERLAP_SEQNUM].data<unsigned int>() = next_index;
  
    Table::insertBuffer(buffer,transactive);
    next_index++;
  }
}
