#include "SFOTZ/DBUpdateInfo.h"

SFOTZ::DBUpdateInfo::DBUpdateInfo()
{
  command=DEFAULT;
  lfn="";
  pfn="";
  filenr=0;
  sfo="";
  sfohost="";
  runnr=0;
  lbnr=0;
  streamtype="";
  stream="";
  guid="";
  filesize=0;
  checksum="";
  nrevents=0;
  sfopfn="";
  projecttag="";
  maxnrsfos=0;
}

SFOTZ::DBUpdateInfo::~DBUpdateInfo()
{
  //noop
}
